﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using DriveHappy.Interface.IServices;
using System.Text;
using System.Configuration;
using DriveHappy.Data.ViewModel;
using DriveHappy.Interface.IRepository;
using DriveHappy.Service.Repository;
using DriveHappy.Data.Models;
using Dapper;
using DriveHappy.Web.Repository;

namespace DriveHappy.Service.Services
{
    /// <summary>
    /// Common Validation Logic
    /// </summary>
    public class CommonServices : ICommonServices
    {
        private IRepository CommonRepo;
        private IHomeRepository HomeRepo = null;
        public CommonServices()
        {
            HomeRepo = new HomeRepository();
            CommonRepo = new CommonRepository();
        }
        /// <summary>
        /// Determines whether [is phone number] [the specified number].
        /// </summary>
        /// <param name="Phone No">The number.</param>
        /// <returns>
        ///   <c>true</c> if [is phone number] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsPhoneNumber(string number)
        {
            if (string.IsNullOrEmpty(number))
                return false;
            if (number.Length == 10)
            {
                long num;
                long.TryParse(number, out num);
                if (num == 0)
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Determines whether the specified email is email.
        /// </summary>
        /// <param name="Email">The email.</param>
        /// <returns>
        ///   <c>true</c> if the specified email is email; otherwise, <c>false</c>.
        /// </returns>
        public bool IsEmail(string Email)
        {
            if (string.IsNullOrEmpty(Email))
                return false;
            string regex = @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$";
            return (Regex.IsMatch(Email, regex));
        }

        /// <summary>
        /// Determines whether [is birth date] [the specified date].
        /// </summary>
        /// <param name="Date">The date.</param>
        /// <returns>
        ///   <c>true</c> if is age is between 70 and 18 [the specified date]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsBirthDate(DateTime Date)
        {
            if (Date == null)
                return false;
            int year = Date.Year;
            int currentYear = DateTime.Now.Year;
            int age = currentYear - year;
            if ((age < 70) && (age > 18))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Determines whether the specified name is name.
        /// </summary>
        /// <param name="Name">The name.</param>
        /// <returns>
        ///   <c>true</c> if the specified name is name; otherwise, <c>false</c>.
        /// </returns>
        public bool IsName(string Name)
        {
            if (string.IsNullOrEmpty(Name))
                return false;
            string regex = "^[a-zA-Z ]+$";
            return (Regex.IsMatch(Name, regex));
        }

        /// <summary>
        /// Determines whether [is strong password] [the specified password].
        /// </summary>
        /// <param name="Password">The password.</param>
        /// <returns>
        ///   <c>true</c> if the password is strong [the specified password]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsStrongPassword(string Password)
        {
            if (string.IsNullOrEmpty(Password))
                return false;
            string regex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}";
            return (Regex.IsMatch(Password, regex));
        }

        // Utility methods
        public static string GetAppSettings(string Key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[Key].ToString();
        }

        /// <summary>
        /// Encrypts the specified message.
        /// </summary>
        /// <param name="Message">The message.</param>
        /// <returns></returns>
        public static string Encrypt(string Message, string Salt)
        {
            return BCrypt.Net.BCrypt.HashPassword(Message, Salt);
        }

        public static string GenerateSalt()
        {
            return BCrypt.Net.BCrypt.GenerateSalt();
        }

        /// <summary>
        /// Determines whether [is alpha numeric] [the specified value].
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [is alpha numeric] [the specified value]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAlphaNumeric(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;
            string regex = @"^[a-zA-Z0-9- ]*$";
            if ((!string.IsNullOrEmpty(value)) && (value.Length < 25))
                return (Regex.IsMatch(value, regex));
            else
                return false;
        }

        /// <summary>
        /// Generates the random alphanumeric.
        /// </summary>
        /// <param name="Size">The size.</param>
        /// <returns></returns>
        public static string GenerateRandomAlphanumeric(int Size)
        {
            Random random = new Random();

            string input = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < Size; i++)
            {
                ch = input[random.Next(0, input.Length)];
                builder.Append(ch);
            }
            return builder.ToString();
        }

        /// <summary>
        /// Generates the name of the file.
        /// </summary>
        /// <returns></returns>
        public static string GenerateFileName()
        {
            int length;
            bool flag = Int32.TryParse(GetAppSettings("fileNameRandomLength"), out length);
            if (length > 0)
                return string.Format(GetAppSettings("fileNameFormat"), DateTime.Now.Ticks.ToString(), GenerateRandomAlphanumeric(length));
            return null;
        }

        /// <summary>
        /// Validates the image.
        /// </summary>
        /// <param name="Image">The image.</param>
        /// <returns></returns>
        public static string ValidateImage(HttpPostedFileBase Image)
        {
            if (Image == null)
            {
                return "Please select the image";
            }
            else if (Image.ContentLength > 0)
            {
                int MB;
                bool flag = Int32.TryParse(ConfigurationManager.AppSettings["maxImageLengthAllowedInMB"], out MB);
                if (flag)
                {
                    int MaxContentLength = 1024 * 1024 * MB; //3 MB
                    string[] AllowedFileExtensions = ConfigurationManager.AppSettings["extentionAllowed"].Trim().ToLower().Split(',');
                    if (!AllowedFileExtensions.Contains(Image.FileName.Substring(Image.FileName.LastIndexOf('.') + 1).ToLower()))
                    {
                        return "File extension jpg, jpeg is supported";
                    }
                    else if (Image.ContentLength > MaxContentLength)
                    {
                        return "File size greater than " + MB + " is not allowed";
                    }
                    return "success";
                }
                return "Image upload failed";
            }
            return "Image upload failed";
        }

        /// <summary>
        /// Validates the time.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <returns></returns>
        public string ValidateTime(DateTime from, DateTime to)
        {
            DateTime leastTime = to.AddHours(-1);
            int validTime = DateTime.Compare(leastTime, from);
            if (validTime > 0)
                return "success";
            return "Minimum time for booking is 1 hr";
        }

        /// <summary>
        /// Validates the car view model.
        /// </summary>
        /// <param name="cvm">The CVM.</param>
        /// <returns></returns>
        public static string ValidateCarViewModel(CarViewModel cvm)
        {
            int seater;
            bool flag = Int32.TryParse(cvm.Seater, out seater);
            if (string.IsNullOrEmpty(cvm.CarName))
            {
                return "Please enter Car Model";
            }
            else if (string.IsNullOrEmpty(cvm.BrandName))
            {
                return "Please enter Brand Name";
            }
            else if (string.IsNullOrEmpty(cvm.CarCategory))
            {
                return "Please Enter Car Category";
            }
            else if (string.IsNullOrEmpty(cvm.FuelType))
            {
                return "Please Enter Fuel Type";
            }
            else if ((string.IsNullOrEmpty(cvm.Seater)) || (!flag))
            {
                return "Please Enter valid Seater";
            }
            else if (string.IsNullOrEmpty(cvm.Transmission))
            {
                return "Please Enter Transmission";
            }
            else
            {
                string output = ValidateImage(cvm.CarImage);
                return output;
            }
        }

        /// <summary>
        /// Validates the owner car data.
        /// </summary>
        /// <param name="ownerCarViewModel">The ownerCarViewModel.</param>
        /// <returns></returns>
        public string ValidateOwnerCarData(OwnerCarViewModel ownerCarViewModel)
        {
            string dateStatus = ValidateDate(ownerCarViewModel.FromDate, ownerCarViewModel.ToDate);
            if (dateStatus.Equals("success"))
            {
                string carDetailStatus = ValidateCarDetails(ownerCarViewModel);
                if (carDetailStatus.Equals("success"))
                {
                    string locationStatus = ValidateLocation(ownerCarViewModel);
                    if (locationStatus.Equals("success"))
                        return "success";
                    return locationStatus;
                }
                return carDetailStatus;
            }
            return dateStatus;
        }


        /// <summary>
        /// Validates the date.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <returns>string message if success or error message</returns>
        public string ValidateDate(DateTime from, DateTime to)
        {
            DateTime addDays = to.AddDays(-2);
            int compare = DateTime.Compare(addDays, from);
            if (compare > 0)
                return "success";
            return "Sorry, Your car should be available to us for minimum 3 day's";
        }

        /// <summary>
        /// Validates the car details.
        /// </summary>
        /// <param name="ownerCarViewModel">The ownerCarViewModel.</param>
        /// <returns></returns>
        public string ValidateCarDetails(OwnerCarViewModel ownerCarViewModel)
        {
            if ((IsAlphaNumeric(ownerCarViewModel.CarModel)) && (IsAlphaNumeric(ownerCarViewModel.CarBrand)) && (IsAlphaNumeric(ownerCarViewModel.PlateNo)))
            {
                int? valueId;
                LookUp lookUp = new LookUp();
                lookUp.LookUpValue = "Brand";
                int? id = CommonRepo.GetId(lookUp, "GetId");
                if (id.HasValue)
                {
                    Value value = new Value();
                    value.ValueName = ownerCarViewModel.CarBrand;
                    value.LookUpId = id ?? 0;
                    valueId = CommonRepo.GetId(value, "GetValueId");
                    if (valueId.HasValue)
                    {
                        int uniquePlateNo = HomeRepo.VerifyPlateNo(ownerCarViewModel.PlateNo);//CommonRepo.ExecuteQuery<int>("SELECT oc.OwnerCarId FROM dbo.OwnerCar oc WHERE oc.PlateNo = '" + ownerCarViewModel.PlateNo + "'");
                        if (uniquePlateNo == 0)
                        {
                            DynamicParameters carDetail = new DynamicParameters();
                            carDetail.Add("@BrandName", valueId);
                            carDetail.Add("@CarName", ownerCarViewModel.CarModel);
                            int? carId = CommonRepo.GetId(carDetail, "GetCarId");
                            if (carId.HasValue)
                            {

                                return "success";
                            }
                            return "Car Model Is Not In Service";
                        }
                        return "Car Already Exist";
                    }
                    return "Car Brand Is Not In Service";
                }

            }
            return "Invalid Input";
        }

        /// <summary>
        /// Validates the user registration.
        /// </summary>
        /// <param name="userViewModel">The userViewModel.</param>
        /// <returns></returns>
        public string ValidateUserRegistration(UserViewModel userViewModel)
        {
            if ((userViewModel.UserId != 2) && (userViewModel.UserId != 3))
            {
                userViewModel.UserId = 3;
            }
            if ((userViewModel.UserId == 2) || (userViewModel.UserId == 3))
            {
                if ((userViewModel.Email != null) && (IsEmail(userViewModel.Email)))
                {
                    int doEmailExist = HomeRepo.VerifyEmail(userViewModel.Email.Trim());
                    if (doEmailExist == 0)
                    {
                        if ((userViewModel.Password != null) && (IsStrongPassword(userViewModel.Password)))
                        {
                            if ((userViewModel.PhoneNo != null) && (IsPhoneNumber(userViewModel.PhoneNo)))
                            {
                                if((DateTime.Now.Year - userViewModel.DOB.Year) < 80)
                                {
                                    DateTime validYear = DateTime.Now.AddYears(-18);
                                    int validPersonAge = DateTime.Compare(userViewModel.DOB, validYear);
                                    if (validPersonAge < 0)
                                        return "success";
                                }
                                return "Age is not valid";
                            }
                            return "Phone no does not exist";
                        }
                        return "Password must be strong";
                    }
                    return "Email already exists";
                }
                return "Email is not valid";
            }
            return "No such user exist";
        }

        /// <summary>
        /// Gets the city.
        /// </summary>
        /// <param name="cityViewModel">The CVM.</param>
        /// <returns>List of cities </returns>
        public List<CityViewModel> GetCity(CityViewModel cityViewModel)
        {
            City city = new City();
            if (cityViewModel != null)
            {
                city.CityName = cityViewModel.CityName;
                List<CityViewModel> listCityViewModel = new List<CityViewModel>();
                List<City> listCity = HomeRepo.GetListCity(city);
                if ((listCity != null) && (listCity.Count > 0))
                {
                    foreach (City c in listCity)
                    {
                        CityViewModel tempCityView = new CityViewModel();
                        tempCityView.CityName = c.CityName;
                        listCityViewModel.Add(tempCityView);
                    }
                    return listCityViewModel;
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the location.
        /// </summary>
        /// <param name="locationHint">The location hint.</param>
        /// <returns>list of location view model</returns>
        public List<LocationViewModel> GetLocation(LocationViewModel locationHint)
        {
            List<LocationViewModel> lcvm = new List<LocationViewModel>();
            List<LocationViewModel> lc = CommonRepo.GetListData<LocationViewModel>(locationHint, "GetLocation");
            if (lc.Count > 0)
            {
                foreach (LocationViewModel c in lc)
                {
                    LocationViewModel ncvm = new LocationViewModel();
                    ncvm.LocationName = c.LocationName.ToUpper();
                    ncvm.LocationId = c.LocationId;
                    lcvm.Add(ncvm);
                }
                return lcvm;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Validates the scheduler.
        /// </summary>
        /// <param name="scheduler">The scheduler.</param>
        /// <returns>if error then error message else success</returns>
        public string ValidateScheduler(SchedulerViewModel scheduler)
        {
            DateTime from = scheduler.FromDate.Date + scheduler.FromTime.TimeOfDay;
            DateTime to = scheduler.ToDate.Date + scheduler.ToTime.TimeOfDay;
            string timeStatus = ValidateUserTime(from, to);
            City city = new City();
            city.CityName = scheduler.CityName;
            if (timeStatus.Equals("success"))
            {
                int? cityId = HomeRepo.GetCityId(city);
                if (cityId.HasValue)
                {
                    Location location = new Location();
                    location.CityId = cityId ?? 0;
                    location.LocationName = scheduler.LocationName;
                    int? locationId = HomeRepo.GetLocationId(location);
                    if (locationId.HasValue)
                    {
                        return "success" + locationId;
                    }
                    return "Location not in service";
                }
                return "City Not In Service";
            }
            return timeStatus;
        }

        /// <summary>
        /// Validates the user time.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <returns>if error then error message else success</returns>
        public string ValidateUserTime(DateTime from, DateTime to)
        {
            if (DateTime.Compare(DateTime.Now, from) <= 0)
            {
                DateTime minimumTime = to.AddHours(-1);
                int compareTimming = DateTime.Compare(minimumTime, from);
                if (compareTimming > 0)
                {
                    DateTime maximumDay = to.AddDays(-6);
                    compareTimming = DateTime.Compare(maximumDay, from);
                    if (compareTimming > 0)
                    {
                        return "Cannot book more than 7 days";
                    }
                    return "success";
                }
                return "Cannot book less than an hour";
            }
            return "Cannot book car for past time";
        }

        /// <summary>
        /// Validates the location.
        /// </summary>
        /// <param name="ownerCarViewModel">The ownerCarViewModel.</param>
        /// <returns></returns>
        public string ValidateLocation(OwnerCarViewModel ownerCarViewModel)
        {
            if ((IsAlphaNumeric(ownerCarViewModel.Location)) && (IsAlphaNumeric(ownerCarViewModel.City)))
            {
                City city = new City();
                city.CityName = ownerCarViewModel.City;
                int? CityId = HomeRepo.GetCityId(city);
                if (CityId.HasValue)
                {
                    Location location = new Location();
                    location.CityId = CityId ?? 0;
                    location.LocationName = ownerCarViewModel.Location;
                    int? LocationId = HomeRepo.GetLocationId(location);
                    if (LocationId.HasValue)
                    {
                        return "success";
                    }
                    return "Location Is Not In Service";
                }
                return "City Is Not In Service";
            }
            return "Invalid Input";
        }

        /// <summary>
        /// Validates the forget password data.
        /// </summary>
        /// <param name="userViewModel">The userViewModel.</param>
        /// <returns>if error then error message else success</returns>
        public string ValidateForgetPasswordData(UserViewModel userViewModel)
        {
            if (IsEmail(userViewModel.Email))
            {
                int? emailId = HomeRepo.GetUserChangePassword(userViewModel.Username, userViewModel.Email);
                if (emailId.HasValue)
                    return "success";
                return "Data does not exist";
            }
            return "Invalid Email";
        }

        /// <summary>
        /// Validates the unique identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>userId</returns>
        public int? ValidateUniqueId(string id)
        {
            int? userId = HomeRepo.GetUserIdByUniqueId(id);
            return userId;
        }
    }
}