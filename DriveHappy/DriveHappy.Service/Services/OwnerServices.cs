﻿using DriveHappy.Data.Models;
using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DriveHappy.Interface.IServices;
using DriveHappy.Interface.IRepository;
using DriveHappy.Service.Repository;
using Dapper;
using DriveHappy.Web.Repository;

namespace DriveHappy.Service.Services
{

    /// <summary>
    /// Class contains owner services
    /// </summary>
    /// <seealso cref="DriveHappy.Interface.IServices.IOwnerServices" />
    public class OwnerServices : IOwnerServices
    {
        private IRepository CommonRepo;
        private IOwnerRepository OwnerRepo;
        public OwnerServices()
        {
            OwnerRepo = new OwnerRepository();
            CommonRepo = new CommonRepository();
        }

        /**
         * Return list of car view model
        */
        public List<CarViewModel> GetCar(int UserId)
        {
            //DynamicParameters userIdParam = new DynamicParameters();
            //userIdParam.Add("@OwnerUserId",UserId);
            List<Car> listCar = OwnerRepo.GetOwnerCar(UserId);//CommonRepo.GetListData<Car>(userIdParam, "GetOwnerCar");
            List<CarViewModel> listCarViewModel = new List<CarViewModel>();
            foreach(Car c in listCar)
            {
                CarViewModel carViewModel = new CarViewModel();
                carViewModel.CarCategory = c.CarCategory;
                carViewModel.CarName = c.CarName;
                carViewModel.FuelType = c.FuelType;
                carViewModel.Seater = c.Seater.ToString();
                carViewModel.Transmission = c.Transmission;
                carViewModel.BrandName = c.BrandName;
                carViewModel.CarPath = CommonServices.GetAppSettings("cdnUrl")+c.CarImage;
                listCarViewModel.Add(carViewModel);
            }
            return listCarViewModel;
        }

        /// <summary>
        /// Adds the owner car.
        /// </summary>
        /// <param name="ownerCarViewModel">The ocvm.</param>
        /// <returns></returns>
        public string AddOwnerCar(OwnerCarViewModel ownerCarViewModel)
        {
            City city = new City();
            city.CityName = ownerCarViewModel.City;
            int? CityId = OwnerRepo.GetCityId(city);//CommonRepo.GetId(city,"GetCityId");
            if(CityId.HasValue)
            {
                Location location = new Location();
                location.CityId = CityId ?? 0;
                location.LocationName = ownerCarViewModel.Location;
                int? LocationId = OwnerRepo.GetLocationId(location);//CommonRepo.GetId(location, "GetLocationId");
                if(LocationId.HasValue)
                {
                    OwnerCar ownerCar = new OwnerCar();
                    ownerCar.UserId = ownerCarViewModel.UserId;
                    ownerCar.AddedOn = DateTime.Now;
                    ownerCar.From = (ownerCarViewModel.FromDate.Date) + (ownerCarViewModel.FromTime.TimeOfDay);
                    ownerCar.To = (ownerCarViewModel.ToDate.Date) + (ownerCarViewModel.ToTime.TimeOfDay);
                    ownerCar.HourlyPrice = ownerCarViewModel.HourlyPrice;
                    ownerCar.DailyPrice = ownerCarViewModel.DailyPrice;
                    ownerCar.SplDayPrice = ownerCarViewModel.SplDayPrice;
                    ownerCar.CarBrand = ownerCarViewModel.CarBrand;
                    ownerCar.CarModel = ownerCarViewModel.CarModel;
                    ownerCar.PlateNo = ownerCarViewModel.PlateNo;
                    ownerCar.CityId = CityId ?? 0;
                    ownerCar.LocationId = LocationId ?? 0;
                    bool flag = CommonRepo.AddSingleRecord(ownerCar, "AddOwnerCar");
                    if (flag)
                        return "success";
                    return "Something went's wrong";
                }
                return "Location Is Not In Service";
            }
            return "City Is Not In Service";
        }

        /// <summary>
        /// Gets the brand name list.
        /// </summary>
        /// <param name="BrandHint">The brand hint.</param>
        /// <returns>List of Brand Name that resembles with BrandHint</returns>
        public List<string> GetBrandNameList(string BrandHint)
        {
            List<string> listBrandName = OwnerRepo.GetBrandNameList(BrandHint);//CommonRepo.ExecuteManyQuery<string>("SELECT v.ValueName FROM dbo.[Value] v INNER JOIN dbo.LookUp lu ON lu.LookUpId = v.LookUpId WHERE lu.LookUpValue = 'Brand' AND v.ValueName LIKE '" + BrandHint + "%'");
            if((listBrandName != null) && (listBrandName.Count > 0))
            {
                return listBrandName;
            }
            return null;
        }

        /// <summary>
        /// Gets the model name list.
        /// </summary>
        /// <param name="ModelHint">The model hint.</param>
        /// <param name="BrandName">Name of the brand.</param>
        /// <returns>List of Model Name that resembles with ModelHint having BrandName specified</returns>
        public List<string> GetModelNameList(string ModelHint, string BrandName)
        {
            List<string> listBrandName = OwnerRepo.GetModelNameList(ModelHint, BrandName);//CommonRepo.ExecuteManyQuery<string>("SELECT c.CarName FROM dbo.[Value] v INNER JOIN dbo.LookUp lu ON lu.LookUpId = v.LookUpId INNER JOIN dbo.Car c ON c.BrandName = v.ValueId WHERE lu.LookUpValue = 'Brand' AND v.ValueName = '" + BrandName + "' AND c.CarName LIKE '" + ModelHint + "%'");
            if ((listBrandName != null) && (listBrandName.Count > 0))
            {
                return listBrandName;
            }
            return null;
        }
    }
}