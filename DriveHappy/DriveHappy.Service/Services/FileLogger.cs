﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DriveHappy.Interface.ILogging;
using System.IO;
using DriveHappy.Service.Services;

namespace DriveHappy.Web.Services
{
    public class FileLogger:ILogBase
    {
        //public string filePath = CommonServices.GetAppSettings("logException") + "LogExceptions.txt";
        public static string path = HttpContext.Current.Server.MapPath("~");
        public static string uploadDirectory = CommonServices.GetAppSettings("logException");
        string docPath = Path.GetFullPath(Path.Combine(path, uploadDirectory));
        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Log(string message)
        {
            using (StreamWriter streamWriter = new StreamWriter(docPath))
            {
                streamWriter.WriteLine(message);
                streamWriter.Close();
            }
        }
    }
}