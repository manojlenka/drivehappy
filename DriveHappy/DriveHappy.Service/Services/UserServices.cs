﻿using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DriveHappy.Interface.IServices;
using DriveHappy.Interface.IRepository;
using DriveHappy.Service.Repository;
using Dapper;
using DriveHappy.Web.Repository;

namespace DriveHappy.Service.Services
{
    public class UserServices:IUserServices
    {
        private IRepository CommonRepo = null;
        private UserRepository UserRepo;
        private ICommonServices commonService = null;
        public UserServices()
        {
            UserRepo = new UserRepository();
            commonService = new CommonServices();
            CommonRepo = new CommonRepository();
        }
        /// <summary>
        /// Gets the car booking details.
        /// </summary>
        /// <param name="ownerCarId">The owner car identifier.</param>
        /// <param name="locationId">The location identifier.</param>
        /// <param name="from">From Timing.</param>
        /// <param name="to">To Timing.</param>
        /// <returns></returns>
        public CarBookingViewModel GetCarBookingDetails(int ownerCarId, int locationId, DateTime from, DateTime to)
        {
            int ownerId = UserRepo.GetOwnerCarId(ownerCarId);//CommonRepo.ExecuteQuery<int>("SELECT oc.OwnerCarId FROM dbo.OwnerCar oc WHERE oc.OwnerCarId = " + ownerCarId);
            if(ownerId != 0)
            {
                int tax = 0;
                DynamicParameters bookingDate = new DynamicParameters();
                bookingDate.Add("@OwnerCarId", ownerCarId);
                bookingDate.Add("@LocationId", locationId);
                CarBookingViewModel carBookingData = UserRepo.GetBookingData(ownerCarId, locationId);//CommonRepo.GetSingleData<CarBookingViewModel>(bookingDate, "GetBookingData");
                carBookingData.From = from;
                carBookingData.To = to;
                int hour = to.TimeOfDay.Hours - from.TimeOfDay.Hours;
                if (hour < 0)
                    hour = hour * -1;
                carBookingData.Day = to.Day - from.Day;
                carBookingData.Hour = hour;
                carBookingData.CarPrice = carBookingData.Day * carBookingData.DailyPrice + carBookingData.Hour * carBookingData.HourlyPrice;
                bool flag = Int32.TryParse(CommonServices.GetAppSettings("TaxPercentage"), out tax);
                if (flag)
                    carBookingData.Tax = ((decimal)tax / 100) * carBookingData.CarPrice;
                carBookingData.NetAmount = ((decimal)(tax + 100) / 100) * carBookingData.CarPrice;
                carBookingData.CarPath = CommonServices.GetAppSettings("cdnUrl") + carBookingData.CarPath;
                return carBookingData;
            }
            return null;
        }
    }
}