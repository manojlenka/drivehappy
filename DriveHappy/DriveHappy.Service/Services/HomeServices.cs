﻿using DriveHappy.Data.Models;
using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DriveHappy.Service.Repository;
using Dapper;
using DriveHappy.Interface.IRepository;
using DriveHappy.Interface.IServices;
using System.Net.Mail;
using System.Text;
using DriveHappy.Web.Repository;

namespace DriveHappy.Service.Services
{
    /**
     * Verifies user credentials
    */
    public class HomeServices : IHomeServices
    {
        private IRepository CommonRepo;
        private IHomeRepository HomeRepo = null;
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeServices"/> class.
        /// </summary>
        public HomeServices()
        {
            HomeRepo = new HomeRepository();
            CommonRepo = new CommonRepository();
        }

        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <param name="loginViewModel">The login view model.</param>
        /// <returns>roles of the respective user</returns>
        public string GetRole(LoginViewModel loginViewModel)
        {
            int? UserId;
            User userModel = new User();
            userModel.Username = loginViewModel.Username;
            userModel.Password = loginViewModel.Password;
            string Salt = HomeRepo.GetSalt(userModel.Username);
            if(!string.IsNullOrEmpty(Salt))
            { 
                DynamicParameters UserLogin = new DynamicParameters();
                UserLogin.Add("@username", userModel.Username);
                string encryptedPassword = CommonServices.Encrypt(userModel.Password, Salt);
                UserLogin.Add("@password", encryptedPassword);
                string RoleName = CommonRepo.GetSingleData<string>(UserLogin, "GetRole");
                if(!string.IsNullOrEmpty(RoleName))
                {
                    UserId = HomeRepo.GetUserId(userModel.Username, encryptedPassword);
                    if(UserId.HasValue)
                        return RoleName+','+UserId;
                }
                return "Error";
            }
            return "Error";
        }

        /// <summary>
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        /// Registering new user
        /// returns name of the user
        public string RegisterUser(UserViewModel userViewModel)
        {
            string uniqueId = (Guid.NewGuid()).ToString();
            string Salt = CommonServices.GenerateSalt();
            string username = userViewModel.Email.Substring(0, userViewModel.Email.IndexOf("@"))+DateTime.Now.Minute;
            userViewModel.Password = CommonServices.Encrypt(userViewModel.Password, Salt);
            bool flag = HomeRepo.RegisterUser(userViewModel, username, Salt, uniqueId);
            if(flag)
            {
                VerifyEmail(userViewModel.Email, uniqueId, username);
            }
            return null;
        }

        /// <summary>
        /// Verifies the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="uniqueId">The unique identifier.</param>
        /// <param name="username">The username.</param>
        private void VerifyEmail(string email, string uniqueId, string username)
        {
            MailMessage mailmessage = new MailMessage("lenka.manoj95@gmail.com", email);
            StringBuilder sbEmailBody = new StringBuilder();
            sbEmailBody.Append("This is a system generated message.<br/>");
            sbEmailBody.Append("If you have registered for the DriveHappy then click the link below to continue. Else don't click the link<br/>");
            sbEmailBody.Append("http://localhost:55568/Home/ActivateAccount?id=" + uniqueId);
            sbEmailBody.Append("<br/>SignIn with username: <b>"+username+"</b>");
            mailmessage.IsBodyHtml = true;
            mailmessage.Body = sbEmailBody.ToString();
            mailmessage.Subject = "Activate Your Account";
            SmtpClient smtpclient = new SmtpClient("smtp.gmail.com", 587);
            smtpclient.Credentials = new System.Net.NetworkCredential()
            {
                UserName = "lenka.manoj95@gmail.com",
                Password = "98068)#(*)maN"
            };

            smtpclient.EnableSsl = true;
            try
            {
                smtpclient.Send(mailmessage);
            }
            catch (Exception) {
                VerifyEmail(email, uniqueId, username);
            }
        }

        /// <summary>
        /// Forgets the password.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="uniqueId">The unique identifier.</param>
        /// <param name="username">The username.</param>
        private void ForgetPassword(string email, string uniqueId)
        {
            MailMessage mailmessage = new MailMessage("lenka.manoj95@gmail.com", email);
            StringBuilder sbEmailBody = new StringBuilder();
            sbEmailBody.Append("This is a system generated message.<br/>");
            sbEmailBody.Append("If you have applied for forget password in the DriveHappy then click the link below to continue. Else don't click the link<br/>");
           
            sbEmailBody.Append("http://localhost:55568/Home/ResetPassword?id=" + uniqueId);
            mailmessage.IsBodyHtml = true;
            mailmessage.Body = sbEmailBody.ToString();
            mailmessage.Subject = "Activate Your Account";
            SmtpClient smtpclient = new SmtpClient("smtp.gmail.com", 587);
            smtpclient.Credentials = new System.Net.NetworkCredential()
            {
                UserName = "lenka.manoj95@gmail.com",
                Password = "98068)#(*)maN"
            };
            smtpclient.EnableSsl = true;
            try
            {
                smtpclient.Send(mailmessage);
            }
            catch (Exception)
            {
                ForgetPassword(email, uniqueId);
            }
        }

        /// <summary>
        /// Confirms the account activation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>boolean if account is valid or not</returns>
        public bool ConfirmAccountActivation(string id)
        {
            DynamicParameters userDetails = new DynamicParameters();
            userDetails.Add("@UniqueId", id);
            DateTime currentTime = DateTime.Now.AddHours(-1);
            userDetails.Add("@CurrentTime", currentTime);
            int? checkAuthentication = CommonRepo.GetId(userDetails, "VerifyUser");
            if(checkAuthentication.HasValue)
                return true;
            return false;
        }

        /// <summary>
        /// Does the forget password.
        /// </summary>
        /// <param name="uvm">The uvm.</param>
        /// <returns>boolean data is correct or not</returns>
        public bool DoForgetPassword(UserViewModel uvm)
        {
            string email = uvm.Email;
            string uniqueId = Guid.NewGuid().ToString();
            //DynamicParameters updateUniqueId = new DynamicParameters();
            //updateUniqueId.Add("@UniqueId", uniqueId);
            //updateUniqueId.Add("@Email", email);
            //updateUniqueId.Add("@CurrentDate", DateTime.Now);
            bool flag = HomeRepo.UpdateUniqueKey(uniqueId, email);//CommonRepo.UpdateRecord(updateUniqueId, "UpdateUniqueKey");
            if(flag)
                ForgetPassword(email, uniqueId);
            return flag;
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>boolean after reseting password</returns>
        public bool ResetPassword(string password, int userId)
        {
            string salt = CommonServices.GenerateSalt();
            string encryptedPassword = CommonServices.Encrypt(password, salt);
            DynamicParameters resetPassword = new DynamicParameters();
            resetPassword.Add("@UserId", userId);
            resetPassword.Add("@Password", encryptedPassword);
            resetPassword.Add("@Salt", salt);
            resetPassword.Add("@CurrentTime", DateTime.Now);
            bool flag = CommonRepo.UpdateRecord(resetPassword, "UpdatePassword");
            return flag;
        }

        /// <summary>
        /// Users the car.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="locationId">The location identifier.</param>
        /// <returns>Return list of user car based on the search</returns>
        public List<UserCarViewModel> UserCar(DateTime from, DateTime to, int locationId)
        {
            int day = 0, hour = 0;
            DynamicParameters listCar = new DynamicParameters();
            listCar.Add("@from", from);
            listCar.Add("@to", to);
            day = (to - from).Days;
            hour = to.TimeOfDay.Hours - from.TimeOfDay.Hours;
            if (hour < 0)
                hour = hour * -1;
            listCar.Add("@locationId", locationId);
            List<UserCar> userCarList = CommonRepo.GetListData<UserCar>(listCar, "GetUserCars");
            List<UserCarViewModel> userCarViewList = new List<UserCarViewModel>();
            foreach(UserCar car in userCarList)
            {
                UserCarViewModel carView = new UserCarViewModel();
                carView.OwnerCarId = car.OwnerCarId;
                carView.BrandName = car.BrandName;
                carView.CarCategory = car.CarCategory;
                carView.CarName = car.CarName;
                carView.CarPath = CommonServices.GetAppSettings("cdnUrl") + car.CarPath;
                carView.FuelType = car.FuelType;
                int seater;
                bool tryToConvert = Int32.TryParse(car.Seater, out seater);
                carView.Seater = seater;
                carView.Transmission = car.Transmission;
                carView.CarPrice = day * car.DailyPrice + hour * car.HourlyPrice;
                userCarViewList.Add(carView);
            }
            return userCarViewList;
        }

        /// <summary>
        /// Gets the car names.
        /// </summary>
        /// <param name="hintCar">The hint car.</param>
        /// <returns>list of car</returns>
        public List<UserCarViewModel> GetCarNames(DateTime from, DateTime to, int locationId, string hintCar)
        {
            int day = 0, hour = 0;
            day = (to - from).Days;
            hour = to.TimeOfDay.Hours - from.TimeOfDay.Hours;
            if (hour < 0)
                hour = hour * -1;
            DynamicParameters searchCar = new DynamicParameters();
            searchCar.Add("@LocationId", locationId);
            searchCar.Add("@AvailableFrom", from);
            searchCar.Add("@AvailableTo", to);
            searchCar.Add("@CarHint", hintCar);
            List<UserCar> listCar = CommonRepo.GetListData<UserCar>(searchCar, "UserSearchList");
            List<UserCarViewModel> listCarView = new List<UserCarViewModel>();
            int seater;
            foreach(UserCar car in listCar)
            {
                UserCarViewModel carView = new UserCarViewModel();
                carView.OwnerCarId = car.OwnerCarId;
                carView.CarPath = CommonServices.GetAppSettings("cdnUrl") + car.CarPath;
                carView.BrandName = car.BrandName;
                carView.CarCategory = car.CarCategory;
                carView.CarName = car.CarName;
                carView.FuelType = car.FuelType;
                if(Int32.TryParse(car.Seater, out seater))
                    carView.Seater = seater;
                carView.Transmission = car.Transmission;
                carView.CarPrice = day * car.DailyPrice + hour * car.HourlyPrice;
                listCarView.Add(carView);
            }
            return listCarView;
        }

        /// <summary>
        /// Validates the filter.
        /// </summary>
        /// <param name="filterName">Name of the filter.</param>
        /// <param name="filterCategory">The filter category.</param>
        /// <param name="listDetails">The list details.</param>
        /// <returns></returns>
        public List<UserCarViewModel> ValidateFilter(DateTime from, DateTime to, int locationId, string filterName)
        {
            DynamicParameters filterType = new DynamicParameters();
            filterType.Add("@from", from);
            filterType.Add("@to", to);
            filterType.Add("@locationId", locationId);
            filterType.Add("@FilterValue", filterName);
            List<UserCarViewModel> filterResults = null;
            List<UserCarViewModel> filterData = new List<UserCarViewModel>();
            if ((filterName.Equals("SUV")) || (filterName.Equals("Hatch back")) || (filterName.Equals("Sedan")) || (filterName.Equals("Luxury")))
            {
                filterResults = CommonRepo.GetListData<UserCarViewModel>(filterType, "GetUserFilterCars");
            }
            else if ((filterName.Equals("Electric")) || (filterName.Equals("Solar")) || (filterName.Equals("Diesel")) || (filterName.Equals("Petrol")))
            {
                filterResults = CommonRepo.GetListData<UserCarViewModel>(filterType, "GetUserFilterCars");
            }
            else if ((filterName.Equals("Manual")) || (filterName.Equals("Auto")) || (filterName.Equals("Dual")))
            {
                filterResults = CommonRepo.GetListData<UserCarViewModel>(filterType, "GetUserFilterCars");
            }
            else if ((filterName.Equals("5")) || (filterName.Equals("4")) || (filterName.Equals("2")) || (filterName.Equals("7")))
            {
                filterResults = CommonRepo.GetListData<UserCarViewModel>(filterType, "GetUserFilterCars");
            }
            if((filterResults != null)&&(filterResults.Count > 0))
            {
                foreach(UserCarViewModel carView in filterResults)
                {
                    carView.CarPath = CommonServices.GetAppSettings("cdnUrl") + carView.CarPath;
                    int day = 0, hour = 0;
                    day = (to - from).Days;
                    hour = to.TimeOfDay.Hours - from.TimeOfDay.Hours;
                    carView.CarPrice = day * carView.DailyPrice + hour * carView.HourlyPrice;
                    filterData.Add(carView);
                }
            }
            return filterData;
        }
    }
}