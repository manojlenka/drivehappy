﻿using DriveHappy.Data.Models;
using DriveHappy.Service.Repository;
using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DriveHappy.Interface.IRepository;
using DriveHappy.Interface.IServices;
using System.IO;
using Dapper;
using DriveHappy.Web.Repository;

namespace DriveHappy.Service.Services
{
    public class AdminServices : IAdminServices
    {
        /// <summary>
        /// The IRepository reference
        /// </summary>
        private IRepository CommonRepo;
        private IAdminRepository AdminRepo;
        /// <summary>
        /// Initializes a new instance of the <see cref="AdminServices"/> class.
        /// Creating reference of the IRepository
        /// </summary>
        public AdminServices()
        {
            AdminRepo = new AdminRepository();
            CommonRepo = new CommonRepository();
        }

        /// <summary>
        /// Methods for adding respective roles
        /// </summary>
        /// <param name="roleViewModel">Array of RoleViewModel</param>
        /// <returns>true if Roles are added successfully</returns>
        public bool AddRole(RoleViewModel[] roleViewModel)
        {
            bool[] flag = new bool[roleViewModel.Length];
            for (int i = 0; i < roleViewModel.Length; i++)
            {
                Role roleModel = new Role();
                roleModel.RoleName = roleViewModel[i].RoleName;
                flag[i] = AdminRepo.AddRole(roleModel);
            }
            return flag[roleViewModel.Length-1];
        }

        /// <summary>
        /// Adds the category.
        /// </summary>
        /// <param name="valueViewModel">The valueViewModel.</param>
        /// <returns>true if Categories are added successfully else false</returns>
        public bool AddCategory(ValueViewModel[] valueViewModel)
        {
            int? id = 0;
            int? LookUpId;
            bool[] flag = new bool[valueViewModel.Length];
            flag[valueViewModel.Length - 1] = false;
            LookUp lookUp = new LookUp();
            lookUp.LookUpValue = "Category";
            LookUpId = AdminRepo.GetId(lookUp);
            if (!LookUpId.HasValue)
            {
                AdminRepo.AddLookUpValue(lookUp);
                LookUpId = AdminRepo.GetId(lookUp);
            }
            for (int i = 0; i < valueViewModel.Length; i++)
            {
                Value valueModel = new Value();
                valueModel.ValueName = valueViewModel[i].ValueName;
                valueModel.LookUpId = LookUpId ?? 0;
                id = AdminRepo.GetValueId(valueModel);
                if (!id.HasValue)
                    flag[i] = AdminRepo.AddCategory(valueModel);
            }
            return flag[valueViewModel.Length-1];
        }

        /// <summary>
        /// Adds the fuel.
        /// </summary>
        /// <param name="valueViewModel">The valueViewModel.</param>
        /// <returns>true if fuel types are added successfully else false</returns>
        public bool AddFuel(ValueViewModel[] valueViewModel)
        {
            int? id = 0;
            int? LookUpId;
            bool[] flag = new bool[valueViewModel.Length];
            flag[valueViewModel.Length - 1] = false;
            LookUp lookUp = new LookUp();
            lookUp.LookUpValue = "Fuel";
            LookUpId = AdminRepo.GetId(lookUp);
            if (!LookUpId.HasValue)
            {
                AdminRepo.AddLookUpValue(lookUp);
                LookUpId = AdminRepo.GetId(lookUp);
            }
            for (int i = 0; i < valueViewModel.Length; i++)
            {
                Value valueModel = new Value();
                valueModel.ValueName = valueViewModel[i].ValueName;
                valueModel.LookUpId = LookUpId ?? 0;
                id = AdminRepo.GetValueId(valueModel);
                if (!id.HasValue)
                    flag[i] = AdminRepo.AddCategory(valueModel);
            }
            return flag[valueViewModel.Length - 1];
        }

        /// <summary>
        /// Adds the seater.
        /// </summary>
        /// <param name="valueViewModel">The valueViewModel.</param>
        /// <returns>true if no. of seats are added successfully else false</returns>
        public bool AddSeater(ValueViewModel[] valueViewModel)
        {
            int? id = 0;
            int? LookUpId;
            bool[] flag = new bool[valueViewModel.Length];
            flag[valueViewModel.Length - 1] = false;
            LookUp lookUp = new LookUp();
            lookUp.LookUpValue = "Seater";
            LookUpId = AdminRepo.GetId(lookUp);
            if (!LookUpId.HasValue)
            {
                AdminRepo.AddLookUpValue(lookUp);
                LookUpId = AdminRepo.GetId(lookUp);
            }
            for (int i = 0; i < valueViewModel.Length; i++)
            {
                Value valueModel = new Value();
                valueModel.ValueName = valueViewModel[i].ValueName;
                valueModel.LookUpId = LookUpId ?? 0;
                id = AdminRepo.GetValueId(valueModel);
                if (!id.HasValue)
                    flag[i] = AdminRepo.AddCategory(valueModel);
            }
            return flag[valueViewModel.Length - 1];
        }

        /// <summary>
        /// Adds the transmission.
        /// </summary>
        /// <param name="valueViewModel">The valueViewModel.</param>
        /// <returns>true if Transmission are added successfully else false</returns>
        public bool AddTransmission(ValueViewModel[] valueViewModel)
        {
            int? id = 0;
            int? LookUpId;
            bool[] flag = new bool[valueViewModel.Length];
            flag[valueViewModel.Length - 1] = false;
            LookUp lookUp = new LookUp();
            lookUp.LookUpValue = "Transmission";
            LookUpId = AdminRepo.GetId(lookUp);
            if (id == 0)
            {
                AdminRepo.AddLookUpValue(lookUp);
                LookUpId = AdminRepo.GetId(lookUp);
            }
            for (int i = 0; i < valueViewModel.Length; i++)
            {
                Value valueModel = new Value();
                valueModel.ValueName = valueViewModel[i].ValueName;
                valueModel.LookUpId = LookUpId ?? 0;
                id = AdminRepo.GetValueId(valueModel);
                if (!id.HasValue)
                    flag[i] = AdminRepo.AddCategory(valueModel);
            }
            return flag[valueViewModel.Length - 1];
        }

        /// <summary>
        /// </summary>
        /// <param name="lookUpView"></param>
        /// <returns>list of view model of the requested
        /// type like transmission, category, fuel type and seater</returns>
        public List<ValueViewModel> GetValue(LookUpViewModel lookUpView)
        {
            LookUp lookUp = new LookUp();
            lookUp.LookUpValue = lookUpView.LookUpValue;
            int? id = 0;
            id = AdminRepo.GetId(lookUp);
            if (id.HasValue)
            {
                Value valueModel = new Value();
                valueModel.LookUpId = id ?? 0;
                List<Value> listValue = AdminRepo.GetValue(valueModel);
                List<ValueViewModel> listValueViewModel = new List<ValueViewModel>();
                if (listValue.Count > 0)
                {
                    foreach (Value v in listValue)
                    {
                        ValueViewModel valueViewModel = new ValueViewModel();
                        valueViewModel.ValueName = v.ValueName;
                        listValueViewModel.Add(valueViewModel);
                    }
                }
                return listValueViewModel;
            }
            return null;
        }

        /// <summary>
        /// Adds the location.
        /// </summary>
        /// <param name="locationViewModel">The LVM.</param>
        /// <returns>true if location is added successfully else false</returns>
        public bool AddLocation(LocationViewModel[] locationViewModel)
        {
            bool[] flag = new bool[locationViewModel.Length];
            string CityName, LocationName;
            for (int i = 0; i < locationViewModel.Length; i++)
            {
                CityName = locationViewModel[i].CityName;
                LocationName = locationViewModel[i].LocationName;
            }
            City cityModel = new City();
            cityModel.CityName = locationViewModel[0].CityName;
            Location[] location = new Location[locationViewModel.Length];
            int? CityId = AdminRepo.GetCityId(cityModel);
            if (!CityId.HasValue)
            {
                AdminRepo.AddCity(cityModel);
                CityId = AdminRepo.GetCityId(cityModel);
            }
            for (int i = 0; i < locationViewModel.Length; i++)
            {
                Location locationModel = new Location();
                locationModel.LocationName = locationViewModel[i].LocationName;
                locationModel.CityId = CityId ?? 0;
                flag[i] = AdminRepo.AddLocation(locationModel);
            }
            return flag[locationViewModel.Length - 1];
        }

        /// <summary>
        /// Gets the city.
        /// </summary>
        /// <param name="carViewModel">The carViewModel.</param>
        /// <returns>CityViewModel</returns>
        public List<CityViewModel> GetCity(CityViewModel carViewModel)
        {
            City cityModel = new City();
            cityModel.CityName = carViewModel.CityName;
            List<CityViewModel> lcarViewModel = new List<CityViewModel>();
            List<City> listCity = AdminRepo.GetCity(cityModel);
            if (listCity.Count > 0)
            {
                foreach (City cityTemp in listCity)
                {
                    CityViewModel ncarViewModel = new CityViewModel();
                    ncarViewModel.CityName = cityTemp.CityName;
                    lcarViewModel.Add(ncarViewModel);
                }
                return lcarViewModel;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Adds the car.
        /// </summary>
        /// <param name="carViewModel">The carViewModel.</param>
        /// <returns>true if data inserted successfully else false</returns>
        public bool AddCar(CarViewModel carViewModel)
        {
            string fileExtention = string.Empty;
            string docPath = string.Empty;
            string path = HttpContext.Current.Server.MapPath("~");
            fileExtention = Path.GetFileName(carViewModel.CarImage.FileName.Substring(carViewModel.CarImage.FileName.LastIndexOf('.')));
            string uploadDirectory = CommonServices.GetAppSettings("dhUploadDir");
            if (uploadDirectory == null)
                return false;
            docPath = Path.GetFullPath(Path.Combine(path, uploadDirectory));
            string newFileName = CommonServices.GenerateFileName() + fileExtention;
            path = Path.Combine(docPath, newFileName);
            Car carModel = new Car();
            carModel.CarCategory = carViewModel.CarCategory;
            carModel.BrandName = carViewModel.BrandName;
            carModel.FuelType = carViewModel.FuelType;
            carModel.Transmission = carViewModel.Transmission;
            int seater;
            bool tryToConvert = Int32.TryParse(carViewModel.Seater, out seater);
            carModel.Seater = seater;
            carModel.CarName = carViewModel.CarName;
            carModel.CarImage = newFileName;
            bool flag = CommonRepo.AddSingleRecord(carModel, "AddCar");
            if(flag)
                carViewModel.CarImage.SaveAs(path);
            return flag;
        }

        /// <summary>
        /// Gets the admin owner data.
        /// </summary>
        /// <returns>List of admin owner data</returns>
        public List<AdminOwnerDataViewModel> GetAdminOwnerData()
        {
            List<AdminOwnerDataViewModel> listAdminOwnerData = CommonRepo.GetListData<AdminOwnerDataViewModel>("GetOwnerVerificationDetail");
            return listAdminOwnerData;
        }

        /// <summary>
        /// Gets the admin car data.
        /// </summary>
        /// <returns>List of Admin Car data</returns>
        public List<AdminCarDataViewModel> GetAdminCarData(int pageNo)
        {
            int beginFrom, endOn;
            int recordPerPage = default(int);
            bool hasRecordsInPage = Int32.TryParse(CommonServices.GetAppSettings("RecordsPerPage"),out recordPerPage);
            if(hasRecordsInPage)
            {
                if(pageNo > 1)
                {
                    beginFrom = (pageNo-1) * recordPerPage;
                    endOn = beginFrom + recordPerPage;
                }
                else
                {
                    beginFrom = 1;
                    endOn = beginFrom + recordPerPage;
                }
                DynamicParameters adminCarData = new DynamicParameters();
                adminCarData.Add("@BeginFrom", beginFrom);
                adminCarData.Add("@EndOn", endOn);
                List<AdminCarDataViewModel> listAdminCarData = CommonRepo.GetListData<AdminCarDataViewModel>(adminCarData, "GetCarVerificationDetail");
                return listAdminCarData;
            }
            return null;
        }


        /// <summary>
        /// Admin car maximum pages.
        /// </summary>
        /// <returns>Max page number</returns>
        public int AdminCarMaxPages()
        {
            int totalPages = default(int);
            int? totalRecord = AdminRepo.CountOwnerCar();
            if(totalRecord.HasValue)
            {
                int recordPerPage;
                bool hasRecordPage = Int32.TryParse(CommonServices.GetAppSettings("RecordsPerPage"), out recordPerPage);
                if(hasRecordPage)
                {
                    if(recordPerPage >= totalRecord)
                    {
                        return 1;
                    }
                    int total = totalRecord ??0;
                    totalPages = (total-1)/recordPerPage;
                    return ( totalPages + 1 );
                }
            }
            return 0;
        }

        /// <summary>
        /// Gets the car.
        /// </summary>
        /// <returns>list of car view model</returns>
        public List<CarViewModel> GetCar()
        {
            List<Car> listCar = AdminRepo.GetCar();
            List<CarViewModel> listCarView = new List<CarViewModel>();
            foreach(Car car in listCar)
            {
                CarViewModel carViewModel = new CarViewModel();
                carViewModel.CarCategory = car.CarCategory;
                carViewModel.CarName = car.CarName;
                carViewModel.FuelType = car.FuelType;
                carViewModel.Seater = car.Seater.ToString();
                carViewModel.Transmission = car.Transmission;
                carViewModel.BrandName = car.BrandName;
                carViewModel.CarPath = CommonServices.GetAppSettings("cdnUrl") + car.CarImage;
                listCarView.Add(carViewModel);
            }
            return listCarView;
        }

        /// <summary>
        /// Gets the car verification view data.
        /// </summary>
        /// <param name="ownerCarId">The owner car identifier.</param>
        /// <returns>Car verification view data</returns>
        public CarVerificationViewModel GetCarVerificationViewData(int ownerCarId)
        {
            List<CarVerificationViewModel> listCarVerificationViewData = AdminRepo.GetCarVerificationDetail(ownerCarId);//CommonRepo.GetListData<CarVerificationViewModel>(carId, "GetCarVerificationViewDetais");
            foreach(CarVerificationViewModel carVerifyData in listCarVerificationViewData)
            {
                carVerifyData.CarPath = CommonServices.GetAppSettings("cdnUrl") + carVerifyData.CarPath;
                return carVerifyData;
            }
            return null;
        }

        /// <summary>
        /// Verifies the car.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns true if data is deleted successfully else false</returns>
        public bool VerifyCar(int id)
        {
            bool flag = AdminRepo.VerifyCar(id);
            return flag;
        }

        /// <summary>
        /// Rejects the car.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns true if data is deleted successfully else false</returns>
        public bool RejectCar(int id)
        {
            bool flag = AdminRepo.RejectCar(id);
            return flag;
        }
    }
}