﻿using DriveHappy.Interface.IRepository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using DriveHappy.Interface.ILogging;
using DriveHappy.Web.Services;
using DriveHappy.Data.ViewModel;
using DriveHappy.Data.Models;

namespace DriveHappy.Web.Repository
{
    public class HomeRepository:IHomeRepository
    {
        private ILogBase logger = null;
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeRepository"/> class.
        /// </summary>
        public HomeRepository()
        {
            logger = new FileLogger();
        }
        /// <summary>
        /// Gets the get connection.
        /// </summary>
        /// <value>
        /// The get connection.
        /// </value>
        public IDbConnection GetConnection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ConnectionString);
            }
        }

        /// <summary>
        /// Gets the salt.
        /// </summary>
        /// <param name="Username">The username.</param>
        /// <returns>Returns salt(string) of specified username</returns>
        public string GetSalt(string Username)
        {
            using(IDbConnection con = GetConnection)
            {
                string query = "SELECT u.Salt FROM dbo.[User] u WHERE u.Username = @Username";
                try
                {
                    con.Open();
                    string salt = con.Query<string>(query, new { Username}).SingleOrDefault<string>();
                    if(!string.IsNullOrEmpty(salt))
                        return salt;
                    return null;
                }
                catch(Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : string GetSalt(string Username), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns>UserId if exist else null</returns>
        public int? GetUserId(string username, string password)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "SELECT u.UserId from dbo.[User] u WHERE u.Username = @username AND u.Password = @password";
                try
                {
                    con.Open();
                    int userId = con.Query<int>(query, new { username, password }).SingleOrDefault<int>();
                    if (userId > 0)
                        return userId;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : int? GetUserId(string username, string password), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Registers the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>true if user is registered successfully else false</returns>
        public bool RegisterUser(UserViewModel user, string Username, string Salt, string UniqueId)
        {
            using (IDbConnection con = GetConnection)
            {
                DateTime CreatedOn = DateTime.Now;
                string query = @"INSERT INTO dbo.[User]
                               (Username, dbo.[User].Password, PhoneNo, Email, RoleId, CreatedOn, UniqueId, dbo.[User].DOB, salt)
                               VALUES (@Username, @Password, @PhoneNo, @Email, @UserId, @CreatedOn, @UniqueId, @DOB, @Salt)";
                try
                {
                    con.Open();
                    con.Query<int>(query, new { Username, user.Password, user.PhoneNo, user.Email, user.UserId, CreatedOn, UniqueId, user.DOB, Salt }).SingleOrDefault<int>();
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : bool RegisterUser(UserViewModel user), Procedure Name: " + query + ", " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Updates the unique key.
        /// </summary>
        /// <param name="UniqueId">The unique identifier.</param>
        /// <param name="Email">The email.</param>
        /// <returns>return true if data is updated successfully else false</returns>
        public bool UpdateUniqueKey(string UniqueId, string Email)
        {
            using (IDbConnection con = GetConnection)
            {
                DateTime CurrentDate = DateTime.Now;
                string query = @"UPDATE dbo.[User]
	                            SET
	                                dbo.[User].UniqueId = @UniqueId,
		                            dbo.[User].CreatedOn = @CurrentDate
	                            WHERE
		                            dbo.[User].Email = @Email";
                try
                {
                    con.Open();
                    con.Query(query, new { UniqueId, CurrentDate, Email });
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : bool UpdateUniqueKey(string UniqueId, string Email), Procedure Name: " + query + ", " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Verifies the plate no.
        /// </summary>
        /// <param name="PlateNo">The plate no.</param>
        /// <returns>plateNo if exist else null</returns>
        public int VerifyPlateNo(string PlateNo)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT oc.OwnerCarId FROM dbo.OwnerCar oc WHERE oc.PlateNo = @PlateNo";
                try
                {
                    con.Open();
                    int userId = con.Query<int>(query, new { PlateNo }).SingleOrDefault<int>();
                    return userId;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : int VerifyPlateNo(string PlateNo), Procedure Name: " + query + ", " + e.Message);
                    return 0;
                }
            }
        }

        /// <summary>
        /// Verifies the email.
        /// </summary>
        /// <param name="Email">The email.</param>
        /// <returns>returns userid if email exist else 0</returns>
        public int VerifyEmail(string Email)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT u.UserId FROM dbo.[User] u WHERE u.Email = @Email";
                try
                {
                    con.Open();
                    int userId = con.Query<int>(query, new { Email }).SingleOrDefault<int>();
                    return userId;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : int VerifyEmail(string Email), Procedure Name: " + query + ", " + e.Message);
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the city identifier.
        /// </summary>
        /// <param name="cityModel">The city model.</param>
        /// <returns>cityId if exist else null</returns>
        public int? GetCityId(City cityModel)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT CityId FROM City WHERE CityName = @Cityname";
                try
                {
                    con.Open();
                    int cityId = con.Query<int>(query, new { cityModel.CityName }).SingleOrDefault<int>();
                    if (cityId > 0)
                        return cityId;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : int? GetCityId(City cityModel), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the location identifier.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns>LocationId if exist else null</returns>
        public int? GetLocationId(Location location)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT l.LocationId from dbo.Location l WHERE l.CityId = @CityId AND l.LocationName = @LocationName";
                try
                {
                    con.Open();
                    int locationId = con.Query<int>(query, new { location.CityId, location.LocationName }).SingleOrDefault<int>();
                    if (locationId > 0)
                        return locationId;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : int? GetLocationId(Location location), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the user change password.
        /// </summary>
        /// <param name="Username">The username.</param>
        /// <param name="Email">The email.</param>
        /// <returns>userId if mail and username exist else null</returns>
        public int? GetUserChangePassword(string Username, string Email)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT u.UserId FROM dbo.[User] u WHERE u.Username = @Username AND u.Email = @Email";
                try
                {
                    con.Open();
                    int userId = con.Query<int>(query, new { Username, Email }).SingleOrDefault<int>();
                    if (userId > 0)
                        return userId;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : int? GetUserChangePassword(string Username, string Email), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the user identifier by unique identifier.
        /// </summary>
        /// <param name="UniqueId">The unique identifier.</param>
        /// <returns>UserId if UniqueId exist else null</returns>
        public int? GetUserIdByUniqueId(string UniqueId)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT TOP 1 u.UserId FROM dbo.[User] u WHERE u. UniqueId = @UniqueId";
                try
                {
                    con.Open();
                    int userId = con.Query<int>(query, new { UniqueId }).SingleOrDefault<int>();
                    if (userId > 0)
                        return userId;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : int? GetUserIdByUniqueId(string UniqueId), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the list city.
        /// </summary>
        /// <param name="city">The city.</param>
        /// <returns>list of city if exist else null</returns>
        public List<City> GetListCity(City city)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT CityName FROM City WHERE CityName LIKE @CityName+'%' ORDER BY CityName";
                try
                {
                    con.Open();
                    List<City> listCity = con.Query<City>(query, new { city.CityName }).ToList<City>();
                    if ((listCity != null) && (listCity.Count > 0))
                        return listCity;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : HomeRespository, At Method : List<City> GetListCity(City city), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }
    }
}