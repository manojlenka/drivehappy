﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using DriveHappy.Interface.ILogging;
using DriveHappy.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DriveHappy.Data.ViewModel;
using DriveHappy.Interface.IRepository;

namespace DriveHappy.Web.Repository
{
    public class UserRepository:IUserRepository
    {
        private ILogBase logger = null;
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        public UserRepository()
        {
            logger = new FileLogger();
        }
        /// <summary>
        /// Gets the get connection.
        /// </summary>
        /// <value>
        /// The get connection.
        /// </value>
        public IDbConnection GetConnection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ConnectionString);
            }
        }

        /// <summary>
        /// Gets the booking data.
        /// </summary>
        /// <param name="OwnerCarId">The owner car identifier.</param>
        /// <param name="LocationId">The location identifier.</param>
        /// <returns>Car Booking View Model data if exist else null</returns>
        public CarBookingViewModel GetBookingData(int OwnerCarId, int LocationId)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT oc.AvailableFrom, oc.AvailableTo, c.CarName, l.LocationName, c.CarImage AS CarPath, v.ValueName AS BrandName, sp.SitePickupId, oc.DailyPrice, oc.HourlyPrice
	                            FROM dbo.OwnerCar AS oc
		                             INNER JOIN
		                             dbo.Car AS c
		                             ON c.CarId = oc.CarId
		                             INNER JOIN
		                             dbo.SitePickup AS sp
		                             ON sp.OwnerCarId = oc.OwnerCarId
		                             INNER JOIN
		                             dbo.Location AS l
		                             ON l.LocationId = sp.LocationId
		                             INNER JOIN dbo.[Value] v ON v.ValueId = c.BrandName
	                            WHERE oc.OwnerCarId = @OwnerCarId AND 
		                              sp.LocationId = @LocationId";
                try
                {
                    con.Open();
                    CarBookingViewModel carBookingView = con.Query<CarBookingViewModel>(query, new { OwnerCarId, LocationId }).SingleOrDefault<CarBookingViewModel>();
                    if (carBookingView != null)
                        return carBookingView;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : UserRespository, At Method : CarBookingViewModel GetBookingData(int OwnerCarId, int LocationId), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the owner car identifier.
        /// </summary>
        /// <param name="OwnerCarId">The owner car identifier.</param>
        /// <returns>OwnerId if exist else 0</returns>
        public int GetOwnerCarId(int OwnerCarId)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT oc.OwnerCarId FROM dbo.OwnerCar oc WHERE oc.OwnerCarId = @OwnerCarId";
                try
                {
                    con.Open();
                    int ownerCarId = con.Query<int>(query, new { OwnerCarId }).SingleOrDefault<int>();
                    return ownerCarId;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : UserRespository, At Method : int? GetOwnerCarId(int OwnerCarId), Procedure Name: " + query + ", " + e.Message);
                    return 0;
                }
            }
        }
    }
}