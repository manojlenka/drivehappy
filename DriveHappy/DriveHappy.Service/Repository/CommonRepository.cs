﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DriveHappy.Data.Models;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using DriveHappy.Interface.IRepository;
using DriveHappy.Interface.ILogging;
using DriveHappy.Web.Services;

namespace DriveHappy.Service.Repository
{
    /**
     * Class for handling login activity of users
    */
    public class CommonRepository : IRepository
    {
        private ILogBase logger = null;
        public CommonRepository()
        {
            logger = new FileLogger();
        }
        /**
         * Get property of connection string 
        */
        public IDbConnection GetConnection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ConnectionString);
            }
        }

        /**
         * Method used for getting the list of records 
         */
        public List<T> GetListData<T>(Object o, string ProcedureName)
        {
            using (IDbConnection con = GetConnection)
            {
                try
                {
                    con.Open();
                    List<T> lv = SqlMapper.Query<T>(con, ProcedureName, o, commandType: CommandType.StoredProcedure).ToList();
                    return lv.ToList();
                }
                catch (Exception e)
                {
                    logger.Log("At Class : CommonRespository At Method : GetListData<T>(Object o, string ProcedureName), Procedure Name: " + ProcedureName + ", " + e.Message );
                    return null;
                }
            }
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <returns>Generic type of data that is requested</returns>
        public T ExecuteQuery<T>(string query)
        {
            using(IDbConnection con = GetConnection)
            {
                try
                {
                    con.Open();
                    return con.Query<T>(query).SingleOrDefault<T>();
                }
                catch (Exception e)
                {
                    logger.Log("At Class : CommonRespository At Method : T ExecuteQuery<T>(string query), Procedure Name: " + query + ", " + e.Message);
                    return default(T);
                }
            }
        }


        /// <summary>
        /// Executes the many query.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <returns>List of data</returns>
        public List<T> ExecuteManyQuery<T>(string query)
        {
            using (IDbConnection con = GetConnection)
            {
                try
                {
                    con.Open();
                    return con.Query<T>(query).ToList();
                }
                catch (Exception e)
                {
                    logger.Log("At Class : CommonRespository At Method : T ExecuteQuery<T>(string query), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }
        /**
         * Returns list of Records
         * By accepting table name
        */
        public List<T> GetListData<T>(string Procedure)
        {
            using(IDbConnection con = GetConnection)
            {
                try
                {
                    con.Open();
                    List<T> lv = SqlMapper.Query<T>(con, Procedure, commandType: CommandType.StoredProcedure).ToList();
                    return lv.ToList();
                }
                catch(Exception e)
                {
                    logger.Log("At Class : CommonRespository At Method : List<T> GetListData<T>(string Procedure), Procedure Name: " + Procedure + ", " + e.Message);
                    return null;
                }
            }
        }

        /**
         * Return id of the record
         * that is searched
        */
        public int? GetId(Object o, string ProcedureName)
        {
            using (IDbConnection con = GetConnection)
            {
                try
                {
                    int? i = SqlMapper.Query<int?>(con, ProcedureName, o, commandType: CommandType.StoredProcedure).SingleOrDefault<int?>();
                    return i;
                }
                catch(Exception e)
                {
                    logger.Log("At Class : CommonRespository At Method : GetId(Object o, string ProcedureName), Procedure Name: " + ProcedureName + ", " + e.Message);
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the single string data from Database.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <param name="ProcedureName">Name of the procedure.</param>
        /// <returns></returns>
        public T GetSingleData<T>(Object o, string ProcedureName)
        {
            using (IDbConnection con = GetConnection)
            {
                try
                {
                    var s = SqlMapper.Query<T>(con, ProcedureName, o, commandType: CommandType.StoredProcedure).SingleOrDefault<T>();
                    return s;
                }
                catch(Exception e)
                {
                    logger.Log("At Class : CommonRespository At Method : GetSingleData<T>(Object o, string ProcedureName), Procedure Name: " + ProcedureName + ", " + e.Message);
                    return default(T);
                }
            }
        }

        /**
         * Method to add record in the table
        */
        public bool AddSingleRecord(Object o, string ProcedureName)
        {
            using (IDbConnection con = GetConnection)
            {
                try
                {
                    con.Execute(ProcedureName, o, commandType: CommandType.StoredProcedure);
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : CommonRespository At Method : AddSingleRecord(Object o, string ProcedureName), Procedure Name: " + ProcedureName + ", " + e.Message);
                    return false;
                }
            }
        }

        public bool UpdateRecord(Object o, string ProcedureName)
        {
            using(IDbConnection con = GetConnection)
            {
                try
                {
                    con.Execute(ProcedureName, o, commandType: CommandType.StoredProcedure);
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : CommonRespository At Method : UpdateRecord(Object o, string ProcedureName), Procedure Name: " + ProcedureName + ", " + e.Message);
                    return false;
                }
            }
        }
    }
}