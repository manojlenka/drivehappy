﻿using DriveHappy.Interface.ILogging;
using DriveHappy.Interface.IRepository;
using DriveHappy.Web.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using DriveHappy.Data.Models;

namespace DriveHappy.Web.Repository
{
    public class OwnerRepository:IOwnerRepository
    {
        private ILogBase logger = null;
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeRepository"/> class.
        /// </summary>
        public OwnerRepository()
        {
            logger = new FileLogger();
        }
        /// <summary>
        /// Gets the get connection.
        /// </summary>
        /// <value>
        /// The get connection.
        /// </value>
        public IDbConnection GetConnection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ConnectionString);
            }
        }

        /// <summary>
        /// Gets the owner car.
        /// </summary>
        /// <param name="OwnerUserId">The owner user identifier.</param>
        /// <returns>list of cars if exist else null</returns>
        public List<Car> GetOwnerCar(int OwnerUserId)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT CarName, CarImage,
	                            v2.ValueName AS CarCategory,
	                            v3.ValueName AS FuelType,
	                            v4.ValueName AS Seater,
	                            v.ValueName AS BrandName,
	                            v5.ValueName AS Transmission
	                            FROM Car c INNER JOIN dbo.OwnerCar oc ON c.CarId = oc.CarId
	                            INNER JOIN dbo.[Value] v ON v.ValueId = c.BrandName
	                            INNER JOIN dbo.[Value] v2 ON v2.ValueId = c.CarCategory
	                            INNER JOIN dbo.[Value] v3 ON v3.ValueId = c.FuelType
	                            INNER JOIN dbo.[Value] v4 ON v4.ValueId = c.Seater
	                            INNER JOIN dbo.[Value] v5 ON v5.ValueId = c.Transmission
	                            WHERE oc.UserId = @OwnerUserId";
                try
                {
                    con.Open();
                    List<Car> listCar = con.Query<Car>(query, new { OwnerUserId }).ToList<Car>();
                    if((listCar != null) && (listCar.Count > 0))
                        return listCar;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : OwnerRepository, At Method : List<Car> GetOwnerCar(int OwnerUserId), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the city identifier.
        /// </summary>
        /// <param name="cityModel">The city model.</param>
        /// <returns>cityId if exist else null</returns>
        public int? GetCityId(City cityModel)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT CityId FROM City WHERE CityName = @Cityname";
                try
                {
                    con.Open();
                    int? cityId = con.Query<int>(query, new { cityModel.CityName }).SingleOrDefault<int>();
                    if ((cityId != null) && (cityId > 0))
                        return cityId;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : OwnerRepository, At Method : int? GetCityId(City cityModel), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the location identifier.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns>LocationId if exist else null</returns>
        public int? GetLocationId(Location location)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT l.LocationId from dbo.Location l WHERE l.CityId = @CityId AND l.LocationName = @LocationName";
                try
                {
                    con.Open();
                    int? locationId = con.Query<int>(query, new { location.CityId, location.LocationName }).SingleOrDefault<int>();
                    if ((locationId != null) && (locationId > 0))
                        return locationId;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : OwnerRepository, At Method : int? GetLocationId(Location location), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the brand name list.
        /// </summary>
        /// <param name="BrandHint">The brand hint.</param>
        /// <returns>List of brand names if exist else null</returns>
        public List<string> GetBrandNameList(string BrandHint)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT v.ValueName FROM dbo.[Value] v INNER JOIN dbo.LookUp lu ON lu.LookUpId = v.LookUpId WHERE lu.LookUpValue = 'Brand' AND v.ValueName LIKE @BrandHint+'%'";
                try
                {
                    con.Open();
                    List<string> listBrandName = con.Query<string>(query, new { BrandHint }).ToList<string>();
                    if ((listBrandName != null) && (listBrandName.Count > 0))
                        return listBrandName;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : OwnerRepository, At Method : List<string> GetBrandNameList(string BrandHint), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the model name list.
        /// </summary>
        /// <param name="ModelHint">The model hint.</param>
        /// <param name="BrandName">Name of the brand.</param>
        /// <returns>List of model names if exists else null</returns>
        public List<string> GetModelNameList(string ModelHint, string BrandName)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT c.CarName FROM dbo.[Value] v INNER JOIN dbo.LookUp lu ON lu.LookUpId = v.LookUpId INNER JOIN dbo.Car c ON c.BrandName = v.ValueId WHERE lu.LookUpValue = 'Brand' AND v.ValueName = @BrandName AND c.CarName LIKE @ModelHint+'%'";
                try
                {
                    con.Open();
                    List<string> listModelName = con.Query<string>(query, new { BrandName, ModelHint }).ToList<string>();
                    if ((listModelName != null) && (listModelName.Count > 0))
                        return listModelName;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : OwnerRepository, At Method : List<string> GetModelNameList(string ModelHint, string BrandName), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }
    }
}