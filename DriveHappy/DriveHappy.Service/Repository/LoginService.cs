﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DriveHappy.DAL.Models;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using DriveHappy.Interface.IRepository;

namespace DriveHappy.Web.Repository
{

    /**
     * Class for handling login activity of users
    */
    public class LoginService : ILogin
    {
        /**
         * Get property of connection string 
        */
        public IDbConnection GetConnection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ConnectionString);
            }
        }


        /**
         * Finding Role of the user from his Username and Password
         * If user is valid then valid role will be returned
         * Else null will be send
         * And List<Role> length will be zero
        */
        public List<Role> GetDetails<Role>(Object o)
        {
            User u = (User)o;
            using (IDbConnection con = GetConnection)
            {
                try
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@username", u.Username);
                    param.Add("@password", u.Password);
                    con.Open();
                    List<Role> getRole = SqlMapper.Query<Role>(con, "GetRole", param, commandType: CommandType.StoredProcedure).ToList();
                    con.Close();
                    return getRole.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}