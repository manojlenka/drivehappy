﻿using DriveHappy.Data.Models;
using DriveHappy.Interface.ILogging;
using DriveHappy.Interface.IRepository;
using DriveHappy.Web.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using DriveHappy.Data.ViewModel;

namespace DriveHappy.Web.Repository
{
    public class AdminRepository:IAdminRepository
    {
        private ILogBase logger = null;
        /// <summary>
        /// Initializes a new instance of the <see cref="AdminRepository"/> class.
        /// </summary>
        public AdminRepository()
        {
            logger = new FileLogger();
        }
        /// <summary>
        /// Gets the get connection.
        /// </summary>
        /// <value>
        /// The get connection.
        /// </value>
        public IDbConnection GetConnection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ConnectionString);
            }
        }

        /// <summary>
        /// Adds the role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>true if role is added successfully else false</returns>
        public bool AddRole(Role role)
        {
            using(IDbConnection con = GetConnection)
            {
                string query = "INSERT INTO Role( RoleName ) VALUES( @RoleName )";
                try
                {
                    con.Open();
                    con.Query(query, new { role.RoleName});
                    return true;
                }
                catch(Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : bool AddRole(Role role), Procedure Name: " + query + ", " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <param name="lookUp">The look up.</param>
        /// <returns>Lookup Id if exist else null</returns>
        public int? GetId(LookUp lookUp)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "SELECT LookUpId FROM LookUp WHERE LookUpValue = @LookUpValue";
                try
                {
                    con.Open();
                    int id = con.Query<int>(query, new { lookUp.LookUpValue }).SingleOrDefault<int>();
                    if(id != 0)
                        return id;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : int? GetId(LookUp lookUp), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Adds the look up value.
        /// </summary>
        /// <param name="lookUp">The look up.</param>
        /// <returns>true if data is successfully inserted else false</returns>
        public bool AddLookUpValue(LookUp lookUp)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "INSERT INTO LookUp( LookUpValue ) VALUES( @LookUpValue )";
                try
                {
                    con.Open();
                    con.Query(query, new { lookUp.LookUpValue });
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : bool AddLookUpValue(LookUp lookUp), Procedure Name: " + query + ", " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the value identifier.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns>value id</returns>
        public int? GetValueId(Value val)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "SELECT ValueId FROM Value WHERE LookUpId = @LookUpId AND ValueName = @ValueName";
                try
                {
                    con.Open();
                    int id = con.Query<int>(query, new { val.LookUpId, val.ValueName }).SingleOrDefault<int>();
                    if (id != 0)
                        return id;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : int? GetId(LookUp lookUp), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Adds the category.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns>true if category is added successfully added else false</returns>
        public bool AddCategory(Value val)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "INSERT INTO Value( LookUpId, ValueName ) VALUES( @LookUpId, @ValueName )";
                try
                {
                    con.Open();
                    con.Query(query, new { val.LookUpId, val.ValueName });
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : bool AddCategory(Value val), Procedure Name: " + query + ", " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="val">The value.</param>
        /// <returns>list of values</returns>
        public List<Value> GetValue(Value val)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "SELECT ValueName FROM Value WHERE LookUpId = @LookUpId";
                try
                {
                    con.Open();
                    List<Value> listValue = con.Query<Value>(query, new { val.LookUpId }).ToList<Value>();
                    if((listValue != null) && (listValue.Count > 0))
                        return listValue;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : List<Value> GetValue(Value val), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the city identifier.
        /// </summary>
        /// <param name="cityModel">The city model.</param>
        /// <returns>cityId if exist else null</returns>
        public int? GetCityId(City cityModel)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "SELECT CityId FROM City WHERE CityName = @Cityname";
                try
                {
                    con.Open();
                    int id = con.Query<int>(query, new { cityModel.CityName }).SingleOrDefault<int>();
                    if (id != 0)
                        return id;
                    return null;
                    
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : int? GetCityId(City cityModel), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Adds the city.
        /// </summary>
        /// <param name="cityModel">The city model.</param>
        /// <returns>true if city is added successfully else false</returns>
        public bool AddCity(City cityModel)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "INSERT INTO City( Cityname ) VALUES( @CityName )";
                try
                {
                    con.Open();
                    con.Query(query, new { cityModel.CityName });
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : bool AddCity(City cityModel), Procedure Name: " + query + ", " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Adds the location.
        /// </summary>
        /// <param name="locationModel">The location model.</param>
        /// <returns>true if location is successfully added else false</returns>
        public bool AddLocation(Location locationModel)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "INSERT INTO Location( CityId, LocationName ) VALUES( @CityId, @LocationName )";
                try
                {
                    con.Open();
                    con.Query(query, new { locationModel.CityId, locationModel.LocationName });
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : bool AddLocation(Location locationModel), Procedure Name: " + query + ", " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the city.
        /// </summary>
        /// <param name="cityModel">The city model.</param>
        /// <returns>list of city names if exist else null</returns>
        public List<City> GetCity(City cityModel)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "SELECT CityName FROM City WHERE CityName LIKE @CityName+'%' ORDER BY CityName";
                try
                {
                    con.Open();
                    List<City> listCity = con.Query<City>(query, new { cityModel.CityName }).ToList<City>();
                    if ((listCity != null) && (listCity.Count > 0))
                        return listCity;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : List<City> GetCity(City cityModel), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Counts the owner car.
        /// </summary>
        /// <returns>number of owner car</returns>
        public int? CountOwnerCar()
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "SELECT Count(oc.OwnerCarId) FROM dbo.OwnerCar oc";
                try
                {
                    con.Open();
                    int id = con.Query<int>(query).SingleOrDefault<int>();
                    if (id != 0)
                        return id;
                    return null;

                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : int? CountOwnerCar(), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the car.
        /// </summary>
        /// <returns>List of cars if exists else null</returns>
        public List<Car> GetCar()
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT CarName, CarImage,
	                            v.ValueName AS CarCategory,
	                            v2.ValueName AS FuelType,
	                            v3.ValueName AS Seater,
	                            v4.ValueName AS BrandName,
	                            v5.ValueName AS Transmission
	                            FROM Car INNER JOIN dbo.[Value] v ON v.ValueId = dbo.Car.CarCategory
	                            INNER JOIN dbo.[Value] v2 ON v2.ValueId = dbo.Car.FuelType
	                            INNER JOIN dbo.[Value] v3 ON v3.ValueId = dbo.Car.Seater
	                            INNER JOIN dbo.[Value] v4 ON v4.ValueId = dbo.Car.BrandName
	                            INNER JOIN dbo.[Value] v5 ON v5.ValueId = dbo.Car.Transmission";
                try
                {
                    con.Open();
                    List<Car> listCar = con.Query<Car>(query).ToList<Car>();
                    if ((listCar != null) && (listCar.Count > 0))
                        return listCar;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : List<Car> GetCar(), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the car verification detail.
        /// </summary>
        /// <param name="OwnerCarId">The owner car identifier.</param>
        /// <returns>list of Car Verification View Model</returns>
        public List<CarVerificationViewModel> GetCarVerificationDetail(int OwnerCarId)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = @"SELECT oc.OwnerCarId, oc.AvailableFrom, oc.AvailableTo, oc.AddedOn, oc.HourlyPrice, oc.DailyPrice, oc.SplDayPrice, u.Username, oc.PlateNo, u.Email, c.CarImage AS CarPath, c.CarName, v.ValueName as BrandName
	                            FROM dbo.OwnerCar AS oc
		                             INNER JOIN
		                             dbo.[User] AS u
		                             ON u.UserId = oc.UserId
		                             INNER JOIN
		                             dbo.Car AS c
		                             ON c.CarId = oc.CarId
		                             INNER JOIN dbo.[Value] v ON v.ValueId = c.BrandName
	                            WHERE oc.OwnerCarId = @OwnerCarId";
                try
                {
                    con.Open();
                    List<CarVerificationViewModel> listCarVerificationData = con.Query<CarVerificationViewModel>(query, new { OwnerCarId }).ToList<CarVerificationViewModel>();
                    if ((listCarVerificationData != null) && (listCarVerificationData.Count > 0))
                        return listCarVerificationData;
                    return null;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : List<CarVerificationViewModel> GetCarVerificationDetail(int OwnerCarId), Procedure Name: " + query + ", " + e.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Verifies the car.
        /// </summary>
        /// <param name="OwnerCarId">The owner car identifier.</param>
        /// <returns>true if car is verified else false</returns>
        public bool VerifyCar(int OwnerCarId)
        {
            using (IDbConnection con = GetConnection)
            {
                string query = "UPDATE dbo.OwnerCar SET dbo.OwnerCar.Verified = 1 WHERE dbo.OwnerCar.OwnerCarId = @OwnerCarId";
                try
                {
                    con.Open();
                    con.Query(query, new { OwnerCarId });
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : bool VerifyCar(int OwnerCarId), Procedure Name: " + query + ", " + e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Rejects the car.
        /// </summary>
        /// <param name="OwnerCarId">The owner car identifier.</param>
        /// <returns>true if record is deleted successfully else false</returns>
        public bool RejectCar(int OwnerCarId)
        {
            using (IDbConnection con = GetConnection)
            {
                string query1 = "DELETE FROM dbo.SitePickup WHERE dbo.SitePickup.OwnerCarId = @OwnerCarId";
                string query2 = "DELETE FROM dbo.OwnerCar WHERE dbo.OwnerCar.OwnerCarId = @OwnerCarId";
                try
                {
                    con.Open();
                    con.Query(query1, new { OwnerCarId });
                    con.Query(query2, new { OwnerCarId });
                    return true;
                }
                catch (Exception e)
                {
                    logger.Log("At Class : AdminRespository, At Method : bool RejectCar(int OwnerCarId), Procedure Name: " + query1 + ", " + e.Message);
                    logger.Log("At Class : AdminRespository, At Method : bool RejectCar(int OwnerCarId), Procedure Name: " + query2 + ", " + e.Message);
                    return false;
                }
            }
        }
    }
}