﻿$(document).ready(function ()
{
    $('.car-add-body-calender').addClass('display-block');
    function showTab(el)
    {
        $('.car-tab').removeClass('display-block');
        $(el).addClass('display-block');
    }
    $('.close-owner-car-add').click(function () {
        closeAddCar();
    });
    function closeAddCar()
    {
        $('.overlay-add-car').css("display", "none");
        $('.car-data-content').empty();
        $('.modify').load('/Owner/MyCars');
    }
    $('.next-ico').click(function () {
        var currentTab = parseInt($('.car-add-body').attr('data-tab'));
        var totalTabs = $('.car-tab').length;
        if ((currentTab >= 0) && (currentTab < totalTabs-1)) {
            if ((currentTab == 0) && (validateDate($('#mytarget').val())) && (validateDate($('#mytargetTo').val())))
            {
                var url = '/Owner/ValidateDate';
                var method = '.DateStatusMessage';
                callValidationMethod(url, method);
            }
            else if ((currentTab == 1) && (validateTime($('#timeFrom').val())) && (validateTime($('#timeTo').val()))) {
                currentTab++;
                nextView(currentTab);
            }
            else if ((currentTab == 2) && (validateMoney($('#hourly-price').val())) && (validateMoney($('#daily-price').val())) && (validateMoney($('#spl-day-price').val()))) {
                currentTab++;
                nextView(currentTab);
            }
            else if ((currentTab == 3) && ($('#car-brand').val() != '') && (validateAlphaNumeric($('#car-brand').val())) && (validateAlphaNumeric($('#model-no').val())) && (validateAlphaNumeric($('#plate-no').val()))) {
                var url = '/Owner/ValidateOwnerCarData';
                var method = '.CarStatusMessage';
                callValidationMethod(url, method);
            }
        }
        else
        {
            var data = $('#ownerCar').serialize();
            var url = $(this).attr('action-url');
            callValidationMethod(url, '.LocationStatusMessage')
        }
    });
    
    function callValidationMethod(url, showMessage)
    {
        var currentTab = parseInt($('.car-add-body').attr('data-tab'));
        var data = $('#ownerCar').serialize();
        var url = url;
        ajaxPost(url, data, function (data, error) {
            if (error) {
                $(showMessage).text(error);
            }
            if (data) {
                currentTab++;
                nextView(currentTab);
                if(currentTab == 4)
                    $('.next-ico').children('span').attr('class', 'fa fa-check');
                if (currentTab == 5)
                    closeAddCar();
            }
        });
    }

    function nextView(currentTab)
    {
        $('.form-circle').removeClass('active-form-circle');
        $($('.form-circle')[currentTab]).addClass('active-form-circle');
        showTab($('.car-tab')[currentTab]);
        $('.car-add-body').attr('data-tab', currentTab);
    }

    function ajaxPost(Url, Message, callback)
    {
        $.ajax({
            type: 'POST',
            url: Url,
            data: Message,
            success: function (res, status, xhr) {
                if(xhr.status == 200)
                {
                    if (callback && (typeof callback === 'function')) {
                        callback(JSON.parse(xhr.responseText).success, null);
                    }
                }
            },
            error: function (xhr, status, error) {
                if(xhr.status == 406)
                {
                    var errorMessage = JSON.parse(xhr.responseText);
                    if (callback && (typeof callback === 'function')) {
                        callback(null, errorMessage.error);
                    };
                }
            }
        });
    }

    function validateDate(value)
    {
        var regex = /(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/\d{4}/;
        if (regex.test(value))
            return true;
        return false;
    }

    function validateTime(value)
    {
        var regex = /^([01]\d|2[0-3]) : ?([0-5]\d)$/;
        if (regex.test(value))
            return true;
        return false;
    }

    function validateMoney(value)
    {
        var regex = /\d+(\.\d{1,2})?/;
        if ((regex.test(value)) && (value.length < 6))
            return true;
        return false;
    }

    function validateAlphaNumeric(value)
    {
        var regex = /^[a-zA-Z0-9 -]*$/;
        if (regex.test(value))
            return true;
        return false;
    }
   /**
   * 
   * This is calendar date picker for the from tab.
   *
   */
    $('#mycalendar').monthly({
        mode: 'picker', // The element that will have its value set to the date you picked
        target: '#mytarget', // Set to true if you want monthly to appear on click
        startHidden: true, // Element that you click to make it appear
        showTrigger: '#mytarget', // Add a style to days in the past
        stylePast: true, // Disable clicking days in the past
        disablePast: true
    });
  /**
  * 
  * This is calendar date picker for the from tab.
  *
  */
    $('#mycalendarTo').monthly({
        mode: 'picker', // The element that will have its value set to the date you picked
        target: '#mytargetTo', // Set to true if you want monthly to appear on click
        startHidden: true, // Element that you click to make it appear
        showTrigger: '#mytargetTo', // Add a style to days in the past
        stylePast: true, // Disable clicking days in the past
        disablePast: true
    });
   /**
   * 
   * This is calendar time picker to show time in 24 hr format.
   *
   */
    $('.timepicker').wickedpicker({
        twentyFour: true
    });
    /**
     * 
     * This is calendar initial time picker value set to blank in our case
     * set to value of placeholder.
     *
     */
    $('.timepicker').val('');
});
$('#car-brand').keyup(function () {
    if ($(this).val().length > 1) {
        $('.car-brand-hint').empty();
        var hint = { BrandHint: $(this).val() };
        var urlPost = '/Owner/GetCarBrand';
        ajaxPost(urlPost, hint, function (data, error) {
            if (data) {
                $('.car-brand-hint').empty();
                for (var i = 0; i < data.length; i++) {
                    addItemToBrandHint(data[i]);
                }
            }
            else {
                
            }
        });
    }
});
function addItemToBrandHint(content) {
    $('.car-brand-hint').append('<div class="car-brand-hint-text">' + content + '</div>');
    $('.car-brand-hint').slideDown('fast', function () {
        $('.car-brand-hint-text').click(function () {
            $('#car-brand').val($(this).html());
            hideCarBrandHint();
        });
    });
}

$('#car-brand').focusout(function () {
    hideCarBrandHint();
});

function hideCarBrandHint() {
    setTimeout(function () {
        $('.car-brand-hint').slideUp('fast');
        $('.car-brand-hint').empty();
    }, 400);
}
function ajaxPost(urlPost, hint, callback) {
    $.ajax({
        type: 'POST',
        url: urlPost,
        contentType: 'application/json',
        data: JSON.stringify(hint),
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                console.log(res.correct);
                try{
                    var result = JSON.parse(res.correct);
                }
                catch(e)
                {
                    result = res.correct;
                }
                if (callback && (typeof callback === 'function'))
                    callback(result, null);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 401) {
                var error = JSON.parse(xhr.responseText);
                if (callback && (typeof callback === 'function'))
                    callback(null, error);
            }
        }
    });
}
/*-----------------------------------------------------------------*/
$('#model-no').keyup(function () {
    if ($(this).val().length > 0) {
        $('.car-model-hint').empty();
        var hint = { ModelHint: $(this).val(), BrandName: $('#car-brand').val() };
        var urlPost = '/Owner/GetCarModel';
        ajaxPost(urlPost, hint, function (data, error) {
            if (data) {
                $('.car-model-hint').empty();
                for (var i = 0; i < data.length; i++) {
                    addItemToModelHint(data[i]);
                }
            }
            else {
                
            }
        });
    }
});
function addItemToModelHint(content) {
    $('.car-model-hint').append('<div class="car-model-hint-text">' + content + '</div>');
    $('.car-model-hint').slideDown('fast', function () {
        $('.car-model-hint-text').click(function () {
            $('#model-no').val($(this).html());
            hideCarModelHint();
        });
    });
}

$('#model-no').focusout(function () {
    hideCarModelHint();
});

function hideCarModelHint() {
    setTimeout(function () {
        $('.car-model-hint').slideUp('fast');
        $('.car-model-hint').empty();
    }, 400);
}