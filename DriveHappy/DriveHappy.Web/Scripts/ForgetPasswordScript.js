﻿$('#verify').click(function (el) {
    el.preventDefault();
    if (VerifyData() == 1) {
        ajaxPost();
    }
});
function VerifyData() {
    if (($('#username').val() == '') && ($('#password').val() == '')) {
        $('#ErrorMessage').text("Fields cannot be left blank").css('color', '#D95D39');
        return 0;
    }
    return 1;
}

function wrapData() {
    var username = $('#username').val();
    var email = $('#Email').val();
    var data = { Username: username, Email: email };
    return data;
}

function ajaxPost() {
    $.ajax({
        type: 'POST',
        url: '/Home/DoForgetPassword',
        contentType: 'application/json',
        data: JSON.stringify(wrapData()),
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                $('#ErrorMessage').text("A verification link is send to your email").css('color', '#136f63');
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 400) {
                $('#ErrorMessage').text("Username or Email does not match").css('color', '#D95D39');
            }
        }
    });
}