﻿$('.close-car-add').click(function () {
    $('.overlay-add-car').fadeOut('fast', function () {
        $('.add-car-data').empty();
    });
});
$('.input-text').focusout(function () {
    if ($(this).val() != '')
        $(this).next('label').addClass('active-add-car-label');
    else
        $(this).next('label').removeClass('active-add-car-label');
});
$(".upload-car-pic").click(function (ev) {
    $("#car-pic").click();
    ev.preventDefault();
    var formData1 = new FormData($('#image-upload')[0]);
    var sendData = { "CarImage": formData1 };
});

$(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);     
        }
    });
});

function imageIsLoaded(el) {
    $('.add-car-head').css('background-image', 'url(' + el.target.result + ')');
};

$('#add-car-save').click(function () {
    var formData1 = new FormData();
    formData1.append("CarImage", $('#car-pic')[0].files[0]);
    formData1.append("CarName", $('#input-car-name').val());
    formData1.append("CarCategory", $('#input-car-category').val());
    formData1.append("FuelType", $('#input-car-fuel').val());
    formData1.append("Seater", $('#input-car-seater').val());
    formData1.append("BrandName", $('#input-car-brand').val());
    formData1.append("Transmission", $('#input-car-transmission').val());
    $.ajax({
        type: "POST",
        url: $(this).attr('call-url'),
        dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        data: formData1,
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                $('.overlay-add-car').fadeOut('fast', function () {
                    $('.add-car-data').empty();
                });
                $('#full-partial-view').load("/Admin/AddCar");
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 406) {
                var response = JSON.parse(xhr.responseText);
                $('#ErrorCarAddition').text(response.error);
            }
        }
    });
});

//$('.input-car-brand').keyup(function () {
//    $('.car-brand-hint-div').empty();
//    var key = $(this).val();
//    if (key != '') {
//        var content = { ValueName: key };
//        //$.ajax({
//        //    type: 'POST',
//        //    url: 'http://localhost:55568/Admin/GetBrand',
//        //    contentType: 'application/json',
//        //    data: JSON.stringify(content),
//        //    success: function (res, status, xhr) {
//        //        if (xhr.status == 200) {
//        //            var response = JSON.parse(res.correct);
//        //            for (var i = 0; i < response.length; i++) {
//        //                //addItemToLocationHint(response[i].CityName);
//        //            }
//        //        }
//        //    },
//        //    error: function (xhr, status, err) {
//        //        if (xhr.status == 404) {
//        //            console.log();
//        //        }
//        //    }
//        //});
//        addItemToBrandHintDiv(key);
//    }
//});

function addItemToBrandDiv(key)
{
    $('#car-brand-hint-div').append();
}