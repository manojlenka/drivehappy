﻿$('.car-data-tr').click(function () {
    var ownerId = { id: $(this).attr('id') };
    ajaxPost(ownerId);
});
function ajaxPost(ownerId) {
    $.ajax({
        type: 'POST',
        url: '/Admin/CarVerification',
        contentType: 'application/json',
        data: JSON.stringify(ownerId),
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                var response = res;
                $('.overlay-verify-car').append(res);
                $('.overlay-verify-car').show('fast');
            }
        },
        error: function (xhr, status, error) { }
    });
}

function updateRecord() {
    for (var i = 0; i <= $('.car-data-tr').length; i++) {
        if ($($('.car-data-tr')[i]).attr('verify-status') == '0') {
            $($('.car-data-tr')[i]).addClass('unverifiedRow');
        }
        else if ($($('.car-data-tr')[i]).attr('verify-status') == '1') {
            $($('.car-data-tr')[i]).removeClass('unverifiedRow');
            $($('.car-data-tr')[i]).addClass('verifiedRow');
        }
    }
}

$(document).ready(function () {
    updateRecord();
    checkDisplay(parseInt($('#car-start-page').val()), parseInt($('.car-end-page').text()))
});

function checkDisplay(current, max)
{
    if(current == 1)
    {
        $('.car-page-previous').css('visibility', 'hidden');
    }
    else if (current > 1)
    {
        $('.car-page-previous').css('visibility', 'visible');
    }
    if(current == max)
    {
        $('.car-page-next').css('visibility', 'hidden');
    }
    else if(current < max)
    {
        $('.car-page-next').css('visibility', 'visible');
    }
}

$('.car-page-next').click(function () {
    var current = parseInt($('#car-start-page').val());
    var maxVal = parseInt($('.car-end-page').text());
    if (current < maxVal)
    {
        var nextVal = current + 1;
        $('#car-start-page').val(nextVal);
        current = nextVal;
    }
    var page = { Id: current };
    ajaxRequest('/Admin/CarView', page, function (data, error) {
        if (data) {
            $('#car-view').empty();
            $('#car-view').append(data);
        }
    });
    checkDisplay(current, maxVal);
});

$('#car-start-page').keyup(function (e) {
    var maxVal = parseInt($('.car-end-page').text());
    var now = parseInt($(this).val());
    if(now > maxVal)
    {
        $(this).val(1);
        checkDisplay(1, maxVal);
    }
    else if(now < 1)
    {
        $(this).val(1);
        checkDisplay(1, maxVal);
    }
    if(e.keyCode == 13)
    {
        var page = {Id: now};
        ajaxRequest('/Admin/CarView', page, function (data, error) {
            if (data) {
                $('#car-view').empty();
                $('#car-view').append(data);
            }
        });
        checkDisplay(now, maxVal);
    }
});

$('.car-page-previous').click(function () {
    var current = parseInt($('#car-start-page').val());
    var maxVal = parseInt($('.car-end-page').text());
    if (current > 1)
    {
        var nextVal = current - 1;
        $('#car-start-page').val(nextVal);
        current = nextVal;
    }
    var page = { Id: current };
    ajaxRequest('/Admin/CarView', page, function (data, error) {
        if (data) {
            $('#car-view').empty();
            $('#car-view').append(data);
        }
    });
    checkDisplay(current, maxVal);
});

function ajaxRequest(link, data, callback)
{
    $.ajax({
        type: 'GET',
        url: link,
        contentType: 'application/json',
        data: data,
        success: function (res, status, xhr) {
            if(xhr.status == 200)
            {
                if(callback && (typeof(callback) === 'function'))
                {
                    var response = xhr.responseText;
                    callback(response, null);
                }
            }
        },
        error: function (xhr, status, error) {
            var error = JSON.parse(xhr.responseText);
            callback(null, error);
        }
    });
}