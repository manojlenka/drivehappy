﻿$('#city').keyup(function () {
    if($('#city').val().length > 1)
    {
        $('.city-hint-text').empty();
        var hint = { CityName: $('#city').val()};
        var urlPost = '/Home/GetCity';
        ajaxPost(urlPost, hint, function (data, error) {
            if(data)
            {
                $('.city-hint-text').empty();
                for (var i = 0; i < data.length; i++)
                {
                    addItemToCityHint(data[i].CityName);
                }
            }
            else
            {
                $('.scheduler-error').text(error);
            }
        });
    }
});

function ajaxPost(urlPost, hint, callback)
{
    $.ajax({
        type: 'POST',
        url: urlPost,
        contentType: 'application/json',
        data: JSON.stringify(hint),
        success: function (res, status, xhr) {
            if(xhr.status == 200)
            {
                var result = JSON.parse(res.correct);
                if (callback && (typeof callback === 'function'))
                    callback(result, null);
            }
        },
        error: function (xhr, status, error) {
            if(xhr.status == 401)
            {
                var error = JSON.parse(xhr.responseText);
                if (callback && (typeof callback === 'function'))
                    callback(null, error);
            }
        }
    });
}

function addItemToCityHint(content) {
    $('.city-hint').append('<li class="city-hint-text">' + content + '</li>');
    $('.city-hint').slideDown('fast', function () {
        $('.city-hint-text').click(function () {
            $('#city').val($(this).html());
            hideCityHint();
        });
    });
}

$('#city').focusout(function () {
    hideCityHint();
});

function hideCityHint() {
    setTimeout(function () {
        $('.city-hint').slideUp('fast');
        $('.city-hint-text').empty();
    }, 400);
}
/*------------------------*/

$('#location').keyup(function () {
    if ($('#location').val().length > 0) {
        $('.location-hint-text').empty();
        var hint = { CityName: $('#city').val(), LocationName: $('#location').val() };
        var urlPost = '/Home/GetLocation';
        ajaxPost(urlPost, hint, function (data, error) {
            if (data) {
                $('.city-hint-text').empty();
                for (var i = 0; i < data.length; i++) {
                    addItemToLocationHint(data[i].LocationName);
                }
            }
        });
    }
});

function addItemToLocationHint(content) {
    $('.location-hint').append('<li class="location-hint-text">' + content + '</li>');
    $('.location-hint').slideDown('fast', function () {
        $('.location-hint-text').click(function () {
            $('#location').val($(this).html());
            hideLocationHint();
        });
    });
}

$('#location').focusout(function () {
    hideLocationHint();
});

function hideLocationHint() {
    setTimeout(function () {
        $('.location-hint').slideUp('fast');
        $('.location-hint-text').empty();
    }, 400);
}
/*------------------------*/
function validateDate(value) {
    var regex = /(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/\d{4}/;
    if (regex.test(value))
        return true;
    $('.scheduler-error').text('Date is not valid');
    return false;
}

function validateTime(value) {
    var regex = /^([01]\d|2[0-3]) : ?([0-5]\d)$/;
    if (regex.test(value))
        return true;
    $('.scheduler-error').text('Time is not valid');
    return false;
}

function validateAlphaNumeric(value) {
    var regex = /^[a-zA-Z0-9 -]*$/;
    if ((regex.test(value)) && (value != ''))
        return true;
    $('.scheduler-error').text('City or location name is invalid');
    return false;
}

function validateScheduler() {
    if ((validateAlphaNumeric($('#city').val())) && (validateAlphaNumeric($('#location').val())) && (validateDate($('#fromDate').val())) && (validateTime($('#fromTime').val())) && (validateDate($('#toDate').val())) && (validateTime($('#toTime').val()))) {
        return true;
    }
    return false;
}
function submitIt() {
    if (validateScheduler())
    {
        var data = {
            CityName: $('#city').val(),
            LocationName: $('#location').val(),
            FromDate: $('#fromDate').val(),
            FromTime: $('#fromTime').val(),
            ToDate: $('#toDate').val(),
            ToTime: $('#toTime').val()
        };

        ajaxRequest('/Home/SchedulerData', data, function (data, error) {
            if (data) {
                window.location.href = '/Home/ShowCar';
            }
            else {
                $('.scheduler-error').text(error.error);
            }
        });
    }
}

function ajaxRequest(urlPost, data, callback) {
    $.ajax({
        type: 'POST',
        url: urlPost,
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                var result = res.correct;
                if (callback && (typeof callback === 'function'))
                    callback(result, null);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 400) {
                var error = JSON.parse(xhr.responseText);
                if (callback && (typeof callback === 'function'))
                    callback(null, error);
            }
        }
    });
}