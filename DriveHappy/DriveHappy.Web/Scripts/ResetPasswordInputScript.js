﻿$(document).ready(function () {
    $('.overlay-login').fadeIn(100);
    $('#reset').click(function(){
        if(($('#password').val() != '')&&($('#confirm-password').val() != ''))
        {
            if(($('#confirm-password').val() == $('#password').val()))
            {
                ajaxResetLogin();
            }
            else
            {
                $('#ErrorMessage').text('Password does not match');
            }
        }
        else
        {
            $('#ErrorMessage').text('Fields cannot be left blank');
        }
    });
});

function ajaxResetLogin()
{
    var passwordData = { password: $('#password').val() };
    $.ajax({
        type: 'POST',
        url: '/Home/DoResetPassword',
        contentType: 'application/json',
        data: JSON.stringify(passwordData),
        success: function (res, status, xhr) {
            console.log(xhr.status);
            if(xhr.status == 200)
            {
                $('#login-partial-content').empty();
                $('#top-bar-link-login').click();
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr.status);
            if(xhr.status == 406)
            {
                var result = JSON.parse(xhr.responseText);
                $('#ErrorMessage').text(result.error);
            }
        }
    });

}