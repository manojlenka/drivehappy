﻿$(document).ready(function () {
    $('.overlay-signup-div .input-text').focusout(function () {
        if ($(this).val() != '') {
            $(this).parent().children('label').addClass('activeLabel');
        }
        else {
            $(this).parent().children('label').removeClass('activeLabel');
        }
    });
    function validateDate(value) {
        var regex = /^\d{2}\/\d{2}\/\d{4}$/;
        if (regex.test(value))
            return true;
        $('.registration-error').text('DOB is not valid');
        return false;
    }

    function validateEmail(value) {
        var regex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        if ((regex.test(value)) && (value.length < 25))
            return true;
        $('.registration-error').text('Email is not valid');
        return false;
    }

    function validatePassword(value) {
        var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}/;
        if (regex.test(value))
            return true;
        $('.registration-error').text('Password must be strong');
        return false;
    }

    function validateTime(value) {
        var regex = /^([01]\d|2[0-3]) : ?([0-5]\d)$/;
        if (regex.test(value))
            return true;
        return false;
    }

    function validateAlphaNumeric(value) {
        var regex = /^[a-zA-Z0-9 -]*$/;
        if (regex.test(value))
            return true;
        return false;
    }
    function validateNumber(value) {
        var regex = /^[0-9]/;
        if ((regex.test(value)) && (value.length > 9) && (value.length < 14))
            return true;
        $('.registration-error').text('Phone no is not valid');
        return false;
    }
    $('.close-signup').click(function () {
        $('.overlay-login').fadeOut('100');
        $('.signup-add').empty();
    });
    function ValidateSignUp()
    {
        if((validateEmail($('#email').val()))&&(validatePassword($('#password').val()))&&(validateNumber($('#phone').val()))&&(validateDate($('#dob').val())))
        {
            return true;
        }
        return false;
    }
    $('#btn-signup-submit').click(function () {
        if (ValidateSignUp())
        {
            var token = $('input[name="__RequestVerificationToken"]').val();
            var Email = $('#email').val();
            var Password = $('#password').val();
            var PhoneNo = $('#phone').val();
            var DOB = $('#dob').val();
            var userId = $('#choose-user').attr('user-val');
            var Details = { __RequestVerificationToken: token, Email: Email, Password: Password, PhoneNo: PhoneNo, DOB: DOB, UserId: userId };
            $.ajax({
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(Details),
                url: "/Home/DoSignUp",
                success: function (res, status, xhr) {
                    if (xhr.status == 200) {
                        $('.registration-error').empty();
                        $('.registration-error').text("Please check your mail");
                        setTimeout(function () {
                            $('.overlay-login').fadeOut('100');
                            $('.signup-add').empty();
                        }, 2000);
                    }
                },
                error: function (xhr, status, error) {
                    if (xhr.status == 406) {
                        var result = JSON.parse(xhr.responseText);
                        $('.registration-error').empty();
                        $('.registration-error').text(result.error);
                    }
                }
            });
        }
    });
});

$('.select-user').click(function () {
    $('.select-owner').removeClass('selected-user');
    $(this).addClass('selected-user');
    $('#choose-user').attr('user-val',3);
});
$('.select-owner').click(function () {
    $('.select-user').removeClass('selected-user');
    $(this).addClass('selected-user');
    $('#choose-user').attr('user-val', 2);
});