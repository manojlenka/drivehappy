function InitAdmin() {
    $('#home').addClass('active-function');
    $('#home-partial-view').show();
    $('#city-view').load("/Admin/CityView");
    $('#city').addClass('active-tab');
    $('#city-view').addClass('active-view');

    (function () {
        ['#home', '#booking', '#transaction', '#application-data', '#add-car', '#city', '#car', '#owner', '#side-bar-toggle', '#pic'].forEach(function (el) {
            $(el).off('click');
        });
    })();

    $('#home').click(function () {
        activeFunction(this);
        activeFunction($('#home'));
        $('#full-partial-view').load("/Admin/HomeView", function() {
            InitAdmin()
        });
        activeTab($('#city'));
        activeView($('#city-view'));
        activeRightView($('#home-partial-view'));
    });
    $('#booking').click(function () {
        activeFunction(this);
    });
    $('#transaction').click(function () {
        activeFunction(this);
    });
    $('#application-data').click(function () {
        $('#full-partial-view').load("/Admin/AppDataView");
        activeFunction(this);
    });
    $('#add-car').click(function () {
        $('#full-partial-view').load("/Admin/AddCar");
        activeFunction(this);
    });
    $('#span').click(function () {
        activeFunction(this);
    });
    $('#city').click(function () {
        $('#city-view').load("/Admin/CityView");
        activeView($('#city-view'));
        activeTab(this);
    });
    $('#car').click(function () {
        $('#car-view').load("/Admin/CarView");
        activeView($('#car-view'));
        activeTab(this);
    });
    $('#owner').click(function () {
        $('#owner-view').load("/Admin/OwnerView");
        activeView($('#owner-view'));
        activeTab(this);
    });

    function activeTab(el) {
        $('#city').removeClass('active-tab');
        $('#car').removeClass('active-tab');
        $('#owner').removeClass('active-tab');
        $(el).addClass('active-tab');
    }

    function activeView(el) {
        $('#city-view').removeClass('active-view');
        $('#car-view').removeClass('active-view');
        $('#owner-view').removeClass('active-view');
        $(el).addClass('active-view');
    }

    function activeFunction(el) {
        $('.admin-function li').removeClass('active-function');
        $(el).addClass('active-function');
    }

    $('#side-bar-toggle').click(function () {
        $('#function-data').slideToggle("fast");
    });
    $('#pic').hover(function () {
        $('#pic-overlay').fadeToggle("fast");
    });
}

$(window).resize(function () { if ($(this).width() > 768) { $(".function-data").slideDown("fast"); } });

function logout() {
    $.ajax({
        url: '/Admin/Logout',

        type: 'GET',

        data: '',

        success: function (res, status, xhr) {
            if (xhr.status === 200) {
                if (res.Redirect) {
                    window.location.href = res.Redirect;
                }
            }
        },

        error: function (xhr, status, error) {

        }
    });
}

function activeRightView(el) {
    $('#home-partial-view').hide();
    $('#appdata-partial-view').hide();
    $('#add-car-partial-view').hide();
    $(el).show();
}