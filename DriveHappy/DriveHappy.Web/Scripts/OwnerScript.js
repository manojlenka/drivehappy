﻿$(document).ready(function () {
    $('.small-screen-ico').click(function () {
        $('.aside').slideToggle('fast');
    });

    $(window).resize(function () {
        if ($(this).width() > 768) {
            $('.aside').show();
        }
    });

    $('#logout').click(function () {
        window.location.href = '/Home/Logout';
    });

    $('.filter-text').click(function () {
        if (!($(this).children('.select-list').hasClass('active-li'))) {
            $('.hidden-filter').slideUp("fast");
            $(this).children('.hidden-filter').slideDown('fast');
            activeList(this);
        } else {
            $(this).children('.select-list').removeClass('active-li');
            $(this).children('.hidden-filter').slideUp('fast');
        }
    });

    $('.owner-text').click(function () {
        activeList(this);
    });

    $('.user-text').click(function () {
        activeList(this);
    });

    function activeList(el)
    {
        $('.filter-type>li').children('.select-list').removeClass('active-li');
        $('.owner>li').children('.select-list').removeClass('active-li');
        $('.my-ride>li').children('.select-list').removeClass('active-li');
        $(el).children('.select-list').addClass('active-li');
    }

    $('.hidden-text').click(function (ev) {
        ev.stopPropagation();
        activeHiddenText(this);
    });

    function activeHiddenText(el)
    {
        $('.hidden-filter').children('li').removeClass('active-li');
        $(el).addClass('active-li');
    }

    $('#home').click(function () {
        $('.modify').empty();
        $('.modify').load('/Home/AllCarAvailable');
    });

    $('#my-cars').click(function () {
        $('.modify').empty();
        $('.modify').load('/Owner/MyCars');
    });

    $('#home').click();

    $('.hidden-text').click(function () {
        var searchItem = $(this).text();
        var searchList = $(this).parent().siblings('.select-list').text().trim();
        var data = { filterName: searchItem, filterCategory: searchList };
        ajaxPartial('/Home/Filter', data, function (data, error) {
            if(data)
            {
                $('.modify').empty();
                $('.modify').append(data.responseText);
            }
            else
            {

            }
        });
    });
});

function ajaxPost(urlPost, hint, callback) {
    $.ajax({
        type: 'POST',
        url: urlPost,
        contentType: 'application/json',
        data: JSON.stringify(hint),
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                var result = JSON.parse(res.correct);
                if (callback && (typeof callback === 'function'))
                    callback(result, null);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 401) {
                var error = JSON.parse(xhr.responseText);
                if (callback && (typeof callback === 'function'))
                    callback(null, error);
            }
        }
    });
}

function ajaxPartial(urlPost, hint, callback) {
    $.ajax({
        type: 'POST',
        url: urlPost,
        contentType: 'application/json',
        data: JSON.stringify(hint),
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                if (callback && (typeof callback === 'function'))
                    callback(xhr, null);
            }
        },
        error: function (xhr, status, error) {
            if ((xhr.status == 401) || (xhr.status == 404)) {
                var error = JSON.parse(xhr.responseText);
                if (callback && (typeof callback === 'function'))
                    callback(null, error);
            }
        }
    });
}

$('.search-input').focusout(function () {
    setTimeout(function () {
        $('.user-search-hint').removeClass('show');
        $('.user-search-hint').empty();
    }, 400);
});

$('.search-input').keyup(function () {
    if($(this).val().length > 1)
    {
        var hint = {hintCar: $(this).val()};
        ajaxPartial('/Home/GetCarName', hint, function (data, error) {
            if (data)
            {
                $('.user-search-hint').removeClass('show');
                $('.user-search-hint').empty();
                if (data.status == 200) {
                    var message = JSON.parse(data.responseText);
                    var result = JSON.parse(message.result);
                    var text;
                    for (var i = 0; i < result.length; i++) {
                        text = 'hint-template-' + i;
                        $('.hint-template').attr('id', text);
                        $('.hint-template').attr('attr-data', result[i].OwnerCarId);
                        $('#' + text).find('.car-name').text(result[i].CarName);
                        $('#' + text).find('.car-search-category').text(result[i].CarCategory);
                        $('#' + text).find('.car-search-fuel').text(result[i].FuelType);
                        $('#' + text).find('.car-search-seater').text(result[i].CarPrice);
                        $('#' + text).find('.car-img-search').attr('style', 'background-image: url(' + result[i].CarPath + ')');
                        $('#' + text).find('.brand-name').text(result[i].BrandName);
                        $('#' + text).find('.car-search-transmission').text(result[i].Transmission);
                        $('.user-search-hint').append($('#' + text).html());
                    }
                    $('.hint').click(function () {
                        $('.search-input').val($(this).find('.car-name').text());
                        var data = { Id: $('.hint-template').attr('attr-data') };
                        ajaxRequest('/Home/BookingDetails', data, function (data, error) {
                            try {
                                    json = $.parseJSON(data);
                                    if(json.code === 302)
                                    {
                                        $('#top-bar-link-login').click();
                                    }
                            }
                            catch (e) {
                                if (data) {
                                    $('.modify').empty();
                                    $('.modify').append(data);
                                }
                                else {
                                    $('.modify').empty();
                                    $('.modify').append(error);
                                }
                            }
                        });
                    });
                    $('.user-search-hint').addClass('show');
                }
            }
            else
            {
                $('.user-search-hint').removeClass('show');
                $('.user-search-hint').empty();
            }
        });
    }
});

function ajaxRequest(link, data, callback) {
    $.ajax({
        url: link,
        type: 'GET',
        contentType: 'application/json',
        data: data,
        success: function (res, status, xhr) {
            if (xhr.status === 200) {
                if (callback && (typeof (callback) === 'function')) {
                    return callback(xhr.responseText, null);
                }
            }
        },
        error: function (xhr, status, error) {
            var error = JSON.parse(xhr.responseText);
            return callback(null, error);
        },
    });
}

function appendText(text)
{
    $('.user-search-hint').append('<div class="selected-hint col-xs-2 clear-padding">' + text + '</div>');
    $('.selected-hint').click(function () {
        $('.search-input').val($(this).text());
    });
}

function valid(el) {
    if ($(el).val() != '') {
        $(el).siblings('label').addClass('active-label');
    }
    else {
        $(el).siblings('label').removeClass('active-label');
    }
}

function addLogin(url) {
    $(".overlay-login>#login-partial-content").empty();
    $(".overlay-login>#login-partial-content").load(url, function () {
        $('.overlay-login').fadeIn(100);
    });
}

function addSignUp(url) {
    $(".overlay-login>#login-partial-content").empty();
    $(".overlay-login>#login-partial-content").load(url, function () {
        $('.overlay-login').fadeIn(100);
    });
}

function loginClose() {
    $('.overlay-login').fadeOut(100);
    $(".overlay-login>#login-partial-content").empty();
}

function validateLoginUser()
{
    if(($('#username').val() != '')&&($('#password').val() != ''))
    {
        return true;
    }
    $('#ErrorMessage').text('Username or password is required');
    return false;
}

function validateAll() {
    if (validateLoginUser())
    {
        var token = $('input[name="__RequestVerificationToken"]').val();
        login({ __RequestVerificationToken: token, Username: $('#username').val(), Password: $('#password').val() });
        return false;
    }
}

function login(credentials) {
    // TODO Add Validation

    $.ajax({
        url: '/Home/_DoLogin',

        type: 'POST',

        data: credentials,

        success: function (res, status, xhr) {
            if (xhr.status === 200) {
                if (res.Redirect) {
                    window.location.href = res.Redirect;
                }
                else
                {
                    loginClose();
                    $('.modify').empty();
                    $('.modify').append(res);
                }
            }
        },

        error: function (xhr, status, error) {
            if (xhr.status === 401) {
                var response = JSON.parse(xhr.responseText);
                $('#ErrorMessage').text(response.errorMismatch);
            }
        }
    });
}