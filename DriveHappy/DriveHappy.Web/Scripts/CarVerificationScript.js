﻿$('.close-container').click(function () {
    closeCarVerification();
    
});
function closeCarVerification() {
    $('.overlay-verify-car').hide('fast');
    $('.overlay-verify-car').empty();
    $('#car-view').load("/Admin/CarView");
    updateRecord();
}
$('.footer-verify').click(function () {
    var passUrl = '/Admin/CarVerified';
    ajaxPost(passUrl);
});
$('.footer-reject').click(function () {
    var passUrl = '/Admin/CarRejected';
    ajaxPost(passUrl);
});

function ajaxPost(passUrl) {
    $.ajax({
        type: 'GET',
        url: passUrl,
        contentType: 'application/json',
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                closeCarVerification();
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 400) {

            }
        }
    });
}