﻿function activeRightView(el) {
    $('#home-partial-view').hide();
    $('#appdata-partial-view').hide();
    $('#add-car-partial-view').hide();
    $(el).show();
}

activeRightView($('#add-car-partial-view'));
$('#add-car-partial-view').show();
$('.overlay-add-car').fadeOut("fast");

$('.car-add-btn').click(function () {
    $('.overlay-add-car').load('/Admin/AddCarInput', function () {
        $('.overlay-add-car').fadeIn("fast");
    });
});