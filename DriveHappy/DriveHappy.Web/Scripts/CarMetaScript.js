﻿
$(document).ready(function () {
    function activeListView(el) {
        var carDetail;
        $('.choice>li').removeClass('active-list-view');
        $(el).addClass('active-list-view');
        $('.CarMetaErrorMessage').empty();
        $('#car-meta-input-content').empty();
        $('#add-category').val('');
        if ($(el).attr('id') == 'fuel') {
            detail = { LookUpValue: "fuel" };
            $('.car-meta-head-content').html('ENTER FUEL TYPES OF CAR');
            addAutoContent(detail);
            $('#save-btn-content').off();
            $('#save-btn-content').click(function () {
                var arr;
                var category = [];
                for (var a = 0; a < $('.selected-car').length; a++) {
                    arr = $($('.selected-car')[a]).html();
                    category[a] = {
                        ValueName: arr
                    }
                }
                $.ajax({
                    type: "POST",
                    url: "/Admin/AddManyFuel",
                    data: JSON.stringify(category),
                    contentType: "application/json",
                    success: function (res, status, xhr) {
                        if (xhr.status == 200) {
                            $('#CarMetaErrorMessage').text(res.correct),
                            $('#CarMetaErrorMessage').css("color", "#63a46c"),
                            $('#car-meta-input-content').empty();
                        }
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status == 409) {
                            var response = JSON.parse(xhr.responseText);
                            $('#CarMetaErrorMessage').text(response.error);
                            $('#CarMetaErrorMessage').css("color", "#f45b69");
                        }
                    }
                })
            });
        }
        else if ($(el).attr('id') == 'category') {
            detail = { LookUpValue: "category" };
            $('.car-meta-head-content').html('ENTER CATEGORIES OF CAR');
            addAutoContent(detail);
            $('#save-btn-content').off();
            $('#save-btn-content').click(function () {
                var arr;
                var category = [];
                for (var a = 0; a < $('.selected-car').length; a++) {
                    arr = $($('.selected-car')[a]).html();
                    category[a] = {
                        ValueName: arr
                    }
                }
                $.ajax({
                    type: "POST",
                    url: "/Admin/AddManyCategory",
                    data: JSON.stringify(category),
                    contentType: "application/json",
                    success: function (res, status, xhr) {
                        if (xhr.status == 200) {
                            $('#CarMetaErrorMessage').text(res.correct),
                            $('#CarMetaErrorMessage').css("color", "#63a46c"),
                            $('#car-meta-input-content').empty();
                        }
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status == 409) {
                            var response = JSON.parse(xhr.responseText);
                            $('#CarMetaErrorMessage').text(response.error);
                            $('#CarMetaErrorMessage').css("color", "#f45b69");
                        }
                    }
                })
            });
        }
        else if ($(el).attr('id') == 'seater') {
            detail = { LookUpValue: "seater" };
            $('.car-meta-head-content').html('ENTER SEATS IN CAR');
            addAutoContent(detail);
            $('#save-btn-content').off();
            $('#save-btn-content').click(function () {
                var arr;
                var category = [];
                for (var a = 0; a < $('.selected-car').length; a++) {
                    arr = $($('.selected-car')[a]).html();
                    category[a] = {
                        ValueName: arr
                    }
                }
                $.ajax({
                    type: "POST",
                    url: "/Admin/AddManySeater",
                    data: JSON.stringify(category),
                    contentType: "application/json",
                    success: function (res, status, xhr) {
                        if (xhr.status == 200) {
                            $('#CarMetaErrorMessage').text(res.correct),
                            $('#CarMetaErrorMessage').css("color", "#63a46c"),
                            $('#car-meta-input-content').empty();
                        }
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status == 409) {
                            var response = JSON.parse(xhr.responseText);
                            $('#CarMetaErrorMessage').text(response.error);
                            $('#CarMetaErrorMessage').css("color", "#f45b69");
                        }
                    }
                })
            });
        }
        else if ($(el).attr('id') == 'transmission') {
            detail = { LookUpValue: "transmission" };
            $('.car-meta-head-content').html('ENTER TRANSMISSION TYPES OF CAR');
            addAutoContent(detail);
            $('#save-btn-content').off();
            $('#save-btn-content').click(function () {
                var arr;
                var category = [];
                for (var a = 0; a < $('.selected-car').length; a++) {
                    arr = $($('.selected-car')[a]).html();
                    category[a] = {
                        ValueName: arr
                    }
                }
                $.ajax({
                    type: "POST",
                    url: "/Admin/AddManyTransmission",
                    data: JSON.stringify(category),
                    contentType: "application/json",
                    success: function (res, status, xhr) {
                        if (xhr.status == 200) {
                            $('#CarMetaErrorMessage').text(res.correct),
                            $('#CarMetaErrorMessage').css("color", "#63a46c"),
                            $('#car-meta-input-content').empty();
                        }
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status == 409) {
                            var response = JSON.parse(xhr.responseText);
                            $('#CarMetaErrorMessage').text(response.error);
                            $('#CarMetaErrorMessage').css("color", "#f45b69");
                        }
                    }
                })
            });
        }
    }
    $('.choice>li').off();
    activeListView($('#category'));
    $('.choice>li').click(function () {
        activeListView('#' + $(this).attr('id'));
    });
    function addSmallTab() {
        if ($('#add-category').val().trim() != '' && $('#add-category').val().trim().length < 25) {
            $('#car-meta-input-content').append('<span class="car-small-tab small-tab"><small class="selected-car">' + $('#add-category').val().trim() + '</small><button class="car-close-small-tab close-small-tab" id="close-small-tab">&times;</button><span>');
            $('.close-small-tab').click(function () {
                $(this).parent().remove();
            });
            $('#add-category').val('');
        }
    }

    $('#add-button').click(function () {
        addSmallTab();
    });

    $('#add-category').keypress(function (ev) {
        var keycode = ev.keyCode ? ev.keyCode : ev.keywhich;
        if (keycode == '13') addSmallTab();
    });

    $('#application-data').click(function () {
        activeAppTab($('#car-meta'));
        $('#car-meta-partial-view').show();
        $('#category').addClass('active-list-view');
        activeRightView($('#appdata-partial-view'));
    });
    function addAutoContent(detail) {
        $.ajax({
            type: "POST",
            url: "/Admin/GetCarMeta",
            data: JSON.stringify(detail),
            contentType: "application/json",
            success: function (res, status, xhr) {
                var response = JSON.parse(res.correct);
                if (xhr.status == 200) {
                    for (var i = 0; i < response.length; i++) {
                        addSmallTabValue(response[i].ValueName);
                    }
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status == 409) {
                    var response = JSON.parse(xhr.responseText);
                }
            }
        })
    }
    function addSmallTabValue(value) {
        $('#car-meta-input-content').append('<span class="car-small-tab small-tab"><small class="selected-car">' + value + '</small><button class="car-close-small-tab close-small-tab" id="close-small-tab">&times;</button><span>');
        $('.close-small-tab').click(function () {
            $(this).parent().remove();
        });
        $('#add-category').val('');
    }
});