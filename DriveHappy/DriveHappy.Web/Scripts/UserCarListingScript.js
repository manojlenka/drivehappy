﻿function bookCar(el) {
    var data = { Id: $(el).attr('btn-id') };
    ajaxRequest('/Home/BookingDetails', data, function (result, error) {
        try {
            json = $.parseJSON(result);
            if(json.code === 302)
            {
                $('#top-bar-link-login').click();
            }
        }
        catch (e) {
            if (result) {
                $('.modify').empty();
                $('.modify').append(result);
            }
            else {
                $('.modify').empty();
                $('.modify').append(error);
            }
        }
    });
}
function ajaxRequest(link, data, callback) {
    $.ajax({
        url: link,
        type: 'GET',
        contentType: 'application/json',
        data: data,
        success: function (res, status, xhr) {
            if (xhr.status === 200) {
                if (callback && (typeof (callback) === 'function')) {
                    return callback(xhr.responseText, null);
                }
            }
        },
        error: function (xhr, status, error) {
            var error = JSON.parse(xhr.responseText);
            return callback(null, error);
        },
    });
}