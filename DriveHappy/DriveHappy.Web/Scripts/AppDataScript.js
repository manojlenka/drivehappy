﻿/**
* 
* Application data view is active
*
*/

activeAppTab($('#car-meta'));
displayPartialView($('#car-meta-partial-view'));
$('#category').addClass('active-list-view');
activeRightView($('#appdata-partial-view'));
$('.role-add').empty();


function activeAppTab(el) {
    $('#role').removeClass('active-appdata-tab');
    $('#car-meta').removeClass('active-appdata-tab');
    $('#location').removeClass('active-appdata-tab');
    $(el).addClass('active-appdata-tab');
}

function displayPartialView(el,callback) {
    $('#car-meta-partial-view').hide();
    $('#role-partial-view').hide();
    $('#location-partial-view').hide();
    $(el).show(function () {
        if (callback) {
            callback();
        }
    });
}

function activeRightView(el) {
    $('#home-partial-view').hide();
    $('#appdata-partial-view').hide();
    $('#add-car-partial-view').hide();
    $(el).show();
}

// Partial Views
$('#role').click(function () {
    $('#role-partial-view').load("/Admin/Role");
    displayPartialView($('#role-partial-view'));
    activeAppTab(this);
});

$('#location').click(function () {
    $('#location-partial-view').load("/Admin/Location");
    displayPartialView($('#location-partial-view'));
    activeAppTab(this);
});

$('#car-meta').click(function () {
    $('#car-meta-partial-view').load("/Admin/CarMeta");
    displayPartialView($('#car-meta-partial-view'));
    activeAppTab(this);
});
