/*
 * File - HomeScript.js
 *
 * Created By - Manoj Lenka
 * Created On - 12/8/2016
 */

$(function () {
    $('#top-bar-link-login').click(function () {
        //$('.overlay-login>#login-partial-content').load('@Url.Action("SignIn", "Login")');
        $('.overlay-login').fadeIn(100);
  });
  $('.login-close').click(function () {
      $('.overlay-login').fadeOut(100);
      $('.overlay-login>#login-partial-content').empty();
  });

  /**
   * 
   * This is calendar date picker for the from tab.
   *
   */
  $('#mycalendar').monthly({
    mode: 'picker', // The element that will have its value set to the date you picked
    target: '#fromDate', // Set to true if you want monthly to appear on click
    startHidden: true, // Element that you click to make it appear
    showTrigger: '#fromDate', // Add a style to days in the past
    stylePast: true, // Disable clicking days in the past
    disablePast: true
  });
  /**
   * 
   * This is calendar date picker for the to tab.
   *
   */
  $('#mycalendar1').monthly({
    mode: 'picker', // The element that will have its value set to the date you picked
    target: '#toDate', // Set to true if you want monthly to appear on click
    startHidden: true, // Element that you click to make it appear
    showTrigger: '#toDate', // Add a style to days in the past
    stylePast: true, // Disable clicking days in the past
    disablePast: true
  });
  /**
   * 
   * This is calendar time picker to show time in 24 hr format.
   *
   */
  $('.timepicker').wickedpicker({
    twentyFour: true
  });
  /**
   * 
   * This is calendar initial time picker value set to blank in our case
   * set to value of placeholder.
   *
   */
  $('.timepicker').val('');
  /**
   * 
   * This is tab indicator when clicked to "from" tab.
   *
   */
  $('#from-indicator').click(function () {
    DisplayFrom();
  });
  /**
   * 
   * This is tab indicator when clicked to "to" tab.
   *
   */
  $('#to-indicator').click(function () {
    DisplayTo();
  });
  /**
   * 
   * This is tab indicator when clicked to "city" tab.
   *
   */
  $('#city-indicator').click(function () {
    DisplayCity();
  });
  /**
   * 
   * This is to shift the tab by clicking next button.
   *
   */
  $('#next').click(function () {
    if ($(this).attr('set-data') == "city") {
        DisplayFrom();
        $(this).attr('set-data', 'from');
    }
    else if ($(this).attr('set-data') == "from") {
      DisplayTo();
      $(this).attr('set-data', 'to');
      $('.next > span').removeClass('fa fa-angle-right');
      $('.next > span').addClass('fa fa-check');
    }
  });
  /**
   * 
   * This will display city tab.
   *
   */
  function DisplayCity() {
    $('#from').fadeOut('fast', function () {
      $('#to').fadeOut('fast', function () {
        $('.city').fadeIn('fast');
        $('#next').attr('set-data', 'city');
      });
    });
    $('.next').attr('onclick', '');
    $('.calendar-wrapper h1').text('Where do you want it');
    $('.city-indicator').addClass('indicator-active');
    $('.to-indicator').removeClass('indicator-active');
    $('.from-indicator').removeClass('indicator-active');
    $('.next > span').removeClass('fa fa-check');
    $('.next > span').addClass('fa fa-angle-right');
  }
  /**
   * 
   * This will display "from" tab.
   *
   */
  function DisplayFrom() {
    $('.city').fadeOut('fast', function () {
      $('#to').fadeOut('fast', function () {
        $('#from').fadeIn('fast');
        $('#next').attr('set-data', 'from');
      });
    });
    $('.calendar-wrapper h1').text('When do you want it');
    $('.from-indicator').addClass('indicator-active');
    $('.to-indicator').removeClass('indicator-active');
    $('.city-indicator').removeClass('indicator-active');
    $('.next > span').removeClass('fa fa-check');
    $('.next > span').addClass('fa fa-angle-right');
    $('.next').attr('onclick', '');
  }

  /**
   * 
   * This will display "to" tab.
   *
   */
  function DisplayTo() {
    $('.city').fadeOut('fast', function () {
      $('#from').fadeOut('fast', function () {
        $('#to').fadeIn('fast');
        $('#next').attr('onclick', 'submitIt()');
      });
    });
    $('.calendar-wrapper h1').text('Okay, till');
    $('.to-indicator').addClass('indicator-active');
    $('.from-indicator').removeClass('indicator-active');
    $('.city-indicator').removeClass('indicator-active');
    $('.next > span').removeClass('fa fa-angle-right');
    $('.next > span').addClass('fa fa-check');
    $('.next').attr('onclick', 1);
  }
});
/**
 *
 * This to make label in top position
 *
 */
function valid(el) {
  if ($(el).val() != '') {
    $(el).siblings('label').addClass('active-label');
  }
  else {
    $(el).siblings('label').removeClass('active-label');
  }
}

function addLogin(url) {
    $(".overlay-login>#login-partial-content").empty();
    $(".overlay-login>#login-partial-content").load(url, function () {
        $('.overlay-login').fadeIn(100);
    });
}

function addSignUp(url) {
    $(".overlay-login>#login-partial-content").empty();
    $(".overlay-login>#login-partial-content").load(url, function () {
        $('.overlay-login').fadeIn(100);
    });
}

function loginClose() {
    $('.overlay-login').fadeOut(100);
    $(".overlay-login>#login-partial-content").empty();
}

function login(credentials) {
    // TODO Add Validation

    $.ajax({
        url : '/Home/_DoLogin',

        type: 'POST',

        data: credentials,

        success: function (res,status,xhr) {
            if (xhr.status === 200) {
                if (res.Redirect) {
                    window.location.href = res.Redirect;
                }
            }
        },

        error: function(xhr,status,error){
            if (xhr.status === 401) {
                var response = JSON.parse(xhr.responseText);
                $('#ErrorMessage').text(response.errorMismatch);
            }
            else if(xhr.status === 500)
            {
                $('html').html(xhr.responseText);
            }
        }
    });

}

$('#logout').click(function () {
    window.location.href = '/Home/Logout';
});

function ValidateLogin()
{
    var UserName = $('#username').val();
    var Password = $('#password').val();
    if ((UserName != '') && (Password != ''))
        return true;
    else
        return false;
}

function validateAll() {
    if (ValidateLogin())
    {
        var token = $('input[name="__RequestVerificationToken"]').val();
        login({ __RequestVerificationToken: token, Username: $('#username').val(), Password: $('#password').val() });
        return false;
    }
    else
    {
        $('#ErrorMessage').text('Username or Password is invalid');
    }
    return false;
}

function validateDate(value) {
    var regex = /(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/\d{4}/;
    if (regex.test(value))
        return true;
    $('.scheduler-error').text('Date is not valid');
    return false;
}

function validateTime(value) {
    var regex = /^([01]\d|2[0-3]) : ?([0-5]\d)$/;
    if (regex.test(value))
        return true;
    $('.scheduler-error').text('Time is not valid');
    return false;
}

function validateAlphaNumeric(value) {
    var regex = /^[a-zA-Z0-9 -]*$/;
    if ((regex.test(value)) && (value != ''))
        return true;
    $('.scheduler-error').text('City or location name is invalid');
    return false;
}


function validateScheduler()
{
    if ((validateAlphaNumeric($('#city').val())) && (validateAlphaNumeric($('#location').val())) && (validateDate($('#fromDate').val())) && (validateTime($('#fromTime').val())) && (validateDate($('#toDate').val())) && (validateTime($('#toTime').val())))
    {
        return true;
    }
    return false;
}

function submitIt() {
    if (validateScheduler())
    {
        var data = {
            CityName: $('#city').val(),
            LocationName: $('#location').val(),
            FromDate: $('#fromDate').val(),
            FromTime: $('#fromTime').val(),
            ToDate: $('#toDate').val(),
            ToTime: $('#toTime').val()
        };
        ajaxRequest('/Home/SchedulerData', data, function (data, error) {
            if (data) {
                window.location.href = '/Home/ShowCar';
            }
            else {
                $('.scheduler-error').text(error.error);
            }
        });
    }
}

function ajaxRequest(urlPost, data, callback) {
    $.ajax({
        type: 'POST',
        url: urlPost,
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                var result = res.correct;
                if (callback && (typeof callback === 'function'))
                    callback(result, null);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 400) {
                var error = JSON.parse(xhr.responseText);
                if (callback && (typeof callback === 'function'))
                    callback(null, error);
            }
        }
    });
}