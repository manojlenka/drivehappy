﻿
$('#role-add-category').keypress(function (ev) {
    var keycode = ev.keyCode ? ev.keyCode : ev.keywhich;
    if (keycode == '13') roleAddSmallTab();
});
$('#role-add-button').click(function () {
    roleAddSmallTab();
});

function roleAddSmallTab() {
    if ($('#role-add-category').val().trim() != '' && $('#role-add-category').val().trim().length < 25) {
        $('#role-input-content').append('<span class="role-small-tab small-tab"><small class="selected-role">' + $('#role-add-category').val().trim() + '</small><button class="role-close-small-tab close-small-tab" id="role-close-small-tab">&times;</button><span>');
        $('.role-close-small-tab').click(function () {
            $(this).parent().remove();
        });
        $('#role-add-category').val('');
    }
}

$('#role-save-btn-content').click(function () {
    var arr;
    var a;
    var sendInfo = [];
    for (var i = 0; i < $('.selected-role').length; i++) {
        arr = $($('.selected-role')[i]).html();
        sendInfo[i] = {
            RoleName: arr
        }
    }
    $.ajax({
        type: "POST",
        url: "/Admin/AddRole",
        data: JSON.stringify(sendInfo),
        contentType: 'application/json',
        success: function (res, status, xhr) {
            if (xhr.status == 200)
            {
                $('#RoleErrorMessage').text(res.correct),
                $('#RoleErrorMessage').css("color", "#63a46c")
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 409)
            {
                var response = JSON.parse(xhr.responseText);
                $('#RoleErrorMessage').text(response.error);
                $('#RoleErrorMessage').css("color", "#f45b69");
            }
        }
    });
});