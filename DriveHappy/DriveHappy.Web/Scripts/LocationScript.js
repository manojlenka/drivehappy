﻿function locationAddSmallTab() {
    if ($('#location-add-category').val().trim() != '' && $('#location-add-category').val().trim().length < 25) {
        $('#location-input-content').append('<span class="location-small-tab small-tab"><small class="selected-location">' + $('#location-add-category').val().trim() + '</small><button class="location-close-small-tab" id="location-close-small-tab">&times;</button><span>');
        $('.location-close-small-tab').click(function () {
            $(this).parent().remove();
        });
        $('#location-add-category').val('');
    }
}
$('#location-add-category').keypress(function (el) {
    var keycode = el.keyCode ? el.keyCode : el.keywhich;
    if (keycode == '13') locationAddSmallTab();
});
$('#location-add-button').click(function () {
    locationAddSmallTab();
});
$('#location-dropdown-button').click(function () {
    locationSlide();
});
$('#location-input-dropdown').keypress(function (el) {
    var keycode = el.keyCode ? el.keyCode : el.keywhich;
    if (keycode == '13') locationSlide();
});

function getLocationContent() {
    var arr;
    var city = $('#city-data').text();
    var content = [];
    for (var i = 0; i < $('.selected-location').length; i++) {
        arr = $($('.selected-location')[i]).text();
        content[i] = {
            "LocationName": arr,
            "CityName": city
        };
    }
    $.ajax({
        type: "POST",
        url: "/Admin/AddLocation",
        data: JSON.stringify(content),
        contentType: "application/json",
        success: function (res, status, xhr) {
            if (xhr.status == 200) {
                $('#ErrorLocationMessage').text(res.correct);
                $('#ErrorLocationMessage').css("color", "#63a46c");
            }
        },
        error: function (xhr, status, err) {
            if (xhr.status == 409) {
                var response = JSON.parse(xhr.responseText);
                $('#ErrorLocationMessage').text(response.error);
                $('#ErrorLocationMessage').css("color", "#f45b69");
            }
        }
    });
}

$('#location-save-btn-content').click(function () {
    getLocationContent();
});

function locationSlide() {
    if ($('#location-input-dropdown').val().trim() != '') {
        $('.location-head-content').html('ENTER LOCATION FOR <span id="city-data">' + $('#location-input-dropdown').val().trim().toUpperCase() + '</span>');
        $('.location-hidden-content').slideDown('fast');
    }
    else {
        $('.location-hidden-content').slideUp('fast');
    }
}

$('#location-input-dropdown').keyup(function () {
    $('.location-hint-content').empty();
    var key = $(this).val();
    if (key != '')
    {
        var content = { "CityName": key };
        $.ajax({
            type: 'POST',
            url: '/Admin/GetCity',
            contentType: 'application/json',
            data: JSON.stringify(content),
            success: function (res, status, xhr) {
                if (xhr.status == 200) {
                    var response = JSON.parse(res.correct);
                    for(var i = 0; i < response.length; i++)
                    {
                        addItemToLocationHint(response[i].CityName);
                    }
                }
            },
            error: function (xhr, status, err) {
                if (xhr.status == 404) {
                    console.log();
                }
            }
        });
    }
});

function addItemToLocationHint(content)
{
    $('.location-hint-content').append('<li class="location-hint-text">' + content + '</li>');
    $('.location-hint').slideDown('fast', function () {
        $('.location-hint-text').click(function () {
            $('#location-input-dropdown').val($(this).html());
            hideLocationHint();
        });
    });
}

$('#location-input-dropdown').focusout(function () {
    hideLocationHint();
});

function hideLocationHint() {
    setTimeout(function () {
        $('.location-hint').slideUp('fast');
        $('.location-hint-content').empty();
    }, 400);
}