﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DriveHappy.Web.CustomAuthentication
{
    public class OwnerAuthorizeFilter : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!Convert.ToBoolean(filterContext.HttpContext.Session["IsOwner"]))
            {
                filterContext.Result = new ContentResult
                {
                    Content = "Unautherized to access specified resource"
                };
            }
        }
    }
}