﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DriveHappy.Web.CustomAuthentication
{
    public class UserAuthorizeFilter: AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!Convert.ToBoolean(filterContext.HttpContext.Session["IsUser"]))
            {
                filterContext.Result = new ContentResult
                {
                    Content = "Unauthorized to access specified resource"
                };
            }
        }
    }
}