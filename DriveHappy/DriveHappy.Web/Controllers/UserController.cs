﻿using DriveHappy.Data.ViewModel;
using DriveHappy.Web.CustomAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DriveHappy.Interface.IServices;
using DriveHappy.Service.Services;

namespace DriveHappy.Web.Controllers
{   
    [UserAuthorizeFilter]
    public class UserController : Controller
    {
        IUserServices UserService = null;
        public UserController()
        {
            UserService = new UserServices();
        }
        /// <summary>
        /// Bookings the details.
        /// </summary>
        /// <returns>Partial view of booking details</returns>
        public ActionResult BookingDetails(int? Id)
        {
            DateTime from = default(DateTime);
            DateTime to = default(DateTime);
            int locationId = default(int);
            int userId = default(int);
            if(Id.HasValue)
            {
                bool flag = DateTime.TryParse(Session["to"].ToString(), out to);
                if (flag)
                    flag = DateTime.TryParse(Session["from"].ToString(), out from);
                if (flag)
                    flag = Int32.TryParse(Session["locationId"].ToString(), out locationId);
                if (flag)
                    flag = Int32.TryParse(Session["UserId"].ToString(), out userId);
                CarBookingViewModel carBooking = UserService.GetCarBookingDetails(Id ?? 0, locationId, from, to);
                if(carBooking == null)
                {
                    return RedirectToAction("CarListing", "Home");
                }
                return PartialView("_BookingDetails", carBooking);
            }
            return RedirectToAction("CarListing", "Home");
        }
    }
}