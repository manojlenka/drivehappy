﻿using DriveHappy.Service.Services;
using DriveHappy.Web.CustomAuthentication;
using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using DriveHappy.Interface.IServices;
using System.Configuration;
using System.IO;

namespace DriveHappy.Web.Controllers
{
    [Authorize]
    [AdminAuthorizeFilter]
    public class AdminController : Controller
    {
        private IAdminServices adminService;
        /// <summary>
        /// Initializes a new instance of the <see cref="AdminController"/> class.
        /// Creates reference for the IAdminServices as AdminService.
        /// </summary>
        public AdminController()
        {
            adminService = new AdminServices();
        }
        // GET: Admin
        /// <summary>
        /// Homes this instance.
        /// </summary>
        /// <returns>Admin Home Page</returns>
        public ActionResult Home()
        {
            return View();
        }

        /// <summary>
        /// Homes the view.
        /// </summary>
        /// <returns>Partial view of admin home page</returns>
        public ActionResult HomeView()
        {
            return PartialView("_HomeView");
        }

        /// <summary>
        /// Applications the data view.
        /// </summary>
        /// <returns>Partial view of admin Application data page</returns>
        public ActionResult AppDataView()
        {
            return PartialView("_AppDataView");
        }

        /// <summary>
        /// Cities the view.
        /// </summary>
        /// <returns>Partial view of admin home page city table view</returns>
        public ActionResult CityView()
        {
            return PartialView("_CityView");
        }

        /// <summary>
        /// Cars the verification.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Partial view for Car verification</returns>
        [HttpPost]
        public ActionResult CarVerification(int? id)
        {
            CarVerificationViewModel carVerification;
            if(id.HasValue)
            {
                carVerification = adminService.GetCarVerificationViewData(id ?? 0);
                Session["OwnerCarId"] = carVerification.OwnerCarId;
            }
            else
                carVerification = new CarVerificationViewModel();
            return PartialView("_CarVerification", carVerification);
        }

        /// <summary>
        /// Cars the verified.
        /// </summary>
        /// <returns></returns>
        public ActionResult CarVerified()
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            int ownerCarId; 
            if(!string.IsNullOrEmpty(Session["OwnerCarId"].ToString()))
            {
                bool valueInInt = Int32.TryParse(Session["OwnerCarId"].ToString(), out ownerCarId);
                if(valueInInt)
                {
                    bool flag = adminService.VerifyCar(ownerCarId);
                    if (flag)
                    {
                        Session["OwnerCarId"] = "";
                        message.Add("code", 200);
                        message.Add("success", ownerCarId);
                    }
                    else
                    {
                        message.Add("code", 400);
                        message.Add("error", "Bad Request");
                        Response.StatusCode = 400;
                        Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }
                else
                {
                    message.Add("code", 400);
                    message.Add("error", "Bad Request");
                    Response.StatusCode = 400;
                    Response.SuppressFormsAuthenticationRedirect = true;
                }
            }
            else
            {
                message.Add("code", 400);
                message.Add("error", "Bad Request");
                Response.StatusCode = 400;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Cars the rejected.
        /// </summary>
        /// <returns>Returns Json result if car is rejected or not</returns>
        public ActionResult CarRejected()
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            int ownerCarId;
            if (!string.IsNullOrEmpty(Session["OwnerCarId"].ToString()))
            {
                bool valueInInt = Int32.TryParse(Session["OwnerCarId"].ToString(), out ownerCarId);
                if (valueInInt)
                {
                    bool flag = adminService.RejectCar(ownerCarId);
                    if (flag)
                    {
                        Session["OwnerCarId"] = "";
                        message.Add("code", 200);
                        message.Add("success", "rejected");
                    }
                    else
                    {
                        message.Add("code", 400);
                        message.Add("error", "Bad Request");
                        Response.StatusCode = 400;
                        Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }
                else
                {
                    message.Add("code", 400);
                    message.Add("error", "Bad Request");
                    Response.StatusCode = 400;
                    Response.SuppressFormsAuthenticationRedirect = true;
                }
            }
            else
            {
                message.Add("code", 400);
                message.Add("error", "Bad Request");
                Response.StatusCode = 400;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Cars the view.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>Partial view of admin home page</returns>
        public ActionResult CarView(int? Id)
        {
            List<AdminCarDataViewModel> listAdminCarData;
            if (!Id.HasValue)
            {
                Id = 1;
            }
            int maxPages = adminService.AdminCarMaxPages();
            if(Id <= maxPages)
            {
                listAdminCarData = adminService.GetAdminCarData(Id ?? 0);
                ViewBag.StartPage = Id ?? 1;
                ViewBag.EndPage = maxPages;
            }
            else
            {
                listAdminCarData = adminService.GetAdminCarData(1);
                ViewBag.StartPage = 1;
                ViewBag.EndPage = maxPages;
            }
            if (listAdminCarData.Count == 0)
            {
                listAdminCarData = new List<AdminCarDataViewModel>();
            }
            return PartialView("_CarView", listAdminCarData);
        }

        /// <summary>
        /// Owners the view.
        /// </summary>
        /// <returns>Partial view of admin home page</returns>
        public ActionResult OwnerView()
        {
            List<AdminOwnerDataViewModel> listAdminOwnerData = adminService.GetAdminOwnerData();
            if(listAdminOwnerData.Count == 0)
            {
                listAdminOwnerData = new List<AdminOwnerDataViewModel>();
            }
            return PartialView("_OwnerView", listAdminOwnerData);
        }

        /// <summary>
        /// Cars the meta.
        /// </summary>
        /// <returns>Partial view of admin Application data page</returns>
        public ActionResult CarMeta()
        {
            return PartialView("_CarMeta");
        }

        /// <summary>
        /// Roles this instance.
        /// </summary>
        /// <returns>Partial view of admin Application data page</returns>
        public ActionResult Role()
        {
            return PartialView("_Role");
        }

        /// <summary>
        /// Locations this instance.
        /// </summary>
        /// <returns>Partial view of admin Application data page</returns>
        public ActionResult Location()
        {
            return PartialView("_Location");
        }
        
        /// <summary>
        /// Adds the role.
        /// </summary>
        /// <param name="roleViewModel">The role view model.</param>
        /// <returns>Json result with message</returns>
        [HttpPost]
        public ActionResult AddRole(RoleViewModel[] roleViewModel)
        {
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            bool flag = adminService.AddRole(roleViewModel);
            if (flag)
            {
                message.Add("code", 200);
                Response.StatusCode = 200;
                message.Add("correct", "Records successfully added");
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
            else
            {
                message.Add("code", 409);
                message.Add("error", "Data entered already exist");
                Response.StatusCode = 409;
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
        }

        /// <summary>
        /// Adds the many category.
        /// </summary>
        /// <param name="valueViewModel">The value view model.</param>
        /// <returns>Json result with message</returns>
        [HttpPost]
        public ActionResult AddManyCategory(ValueViewModel[] valueViewModel)
        {
            bool flag = adminService.AddCategory(valueViewModel);
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if (flag)
            {
                message.Add("code", 200);
                Response.StatusCode = 200;
                message.Add("correct", "Records successfully added");
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
            else
            {
                message.Add("code", 409);
                message.Add("error", "Data entered already exist");
                Response.StatusCode = 409;
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
        }

        /// <summary>
        /// Adds the many fuel.
        /// </summary>
        /// <param name="valueViewModel">The value view model.</param>
        /// <returns>Json result with message</returns>
        [HttpPost]
        public ActionResult AddManyFuel(ValueViewModel[] valueViewModel)
        {
            bool flag = adminService.AddFuel(valueViewModel);
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if (flag)
            {
                message.Add("code", 200);
                Response.StatusCode = 200;
                message.Add("correct", "Records successfully added");
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
            else
            {
                message.Add("code", 409);
                message.Add("error", "Data entered already exist");
                Response.StatusCode = 409;
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
        }

        /// <summary>
        /// Adds the many seater.
        /// </summary>
        /// <param name="valueViewModel">The value view model.</param>
        /// <returns>Json result with message</returns>
        [HttpPost]
        public ActionResult AddManySeater(ValueViewModel[] valueViewModel)
        {
            bool flag = adminService.AddSeater(valueViewModel);
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if (flag)
            {
                message.Add("code", 200);
                Response.StatusCode = 200;
                message.Add("correct", "Records successfully added");
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
            else
            {
                message.Add("code", 409);
                message.Add("error", "Data entered already exist");
                Response.StatusCode = 409;
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
        }

        /// <summary>
        /// Adds the many transmission.
        /// </summary>
        /// <param name="valueViewModel">The VVM.</param>
        /// <returns>JSON Response</returns>
        [HttpPost]
        public ActionResult AddManyTransmission(ValueViewModel[] valueViewModel)
        {
            bool flag = adminService.AddTransmission(valueViewModel);
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if (flag)
            {
                message.Add("code", 200);
                Response.StatusCode = 200;
                message.Add("correct", "Records successfully added");
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
            else
            {
                message.Add("code", 409);
                message.Add("error", "Data entered already exist");
                Response.StatusCode = 409;
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
        }

        /// <summary>
        /// Gets the car meta.
        /// </summary>
        /// <param name="lookUpViewModel">The look up view model.</param>
        /// <returns>Json result with message</returns>
        [HttpPost]
        public ActionResult GetCarMeta(LookUpViewModel lookUpViewModel)
        {
            List<ValueViewModel> listValueViewModel = adminService.GetValue(lookUpViewModel) ?? null;
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if (listValueViewModel.Count > 0)
            {
                var jsonSerialiser = new JavaScriptSerializer();
                string json = jsonSerialiser.Serialize(listValueViewModel);
                message.Add("code", 200);
                message.Add("correct", json);
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
            else
            {
                message.Add("code", 404);
                message.Add("error", "Could not found any data");
                Response.SuppressFormsAuthenticationRedirect = true;
                result = Json(message, JsonRequestBehavior.AllowGet);
                return result;
            }
        }

        /// <summary>
        /// Adds the location.
        /// </summary>
        /// <param name="locationViewModel">The LVM.</param>
        /// <returns>json result</returns>
        [HttpPost]
        public ActionResult AddLocation(LocationViewModel[] locationViewModel)
        {
            bool flag = adminService.AddLocation(locationViewModel);
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            if (flag)
            {
                message.Add("code", 200);
                message.Add("correct", "Data Inserted successfully");
                result = Json(message, JsonRequestBehavior.AllowGet);
                return result;
            }
            else
            {
                message.Add("code", 409);
                Response.StatusCode = 409;
                message.Add("error", "Data Inserted already exist");
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
        }

        /// <summary>
        /// Gets the city.
        /// </summary>
        /// <param name="cityViewModel">The CVM.</param>
        /// <returns>Json result</returns>
        [HttpPost]
        public ActionResult GetCity(CityViewModel cityViewModel)
        {
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if (cityViewModel.CityName.Length >= 2)
            {
                List<CityViewModel> listCities = adminService.GetCity(cityViewModel);
                var jsonSerialiser = new JavaScriptSerializer();
                string json = jsonSerialiser.Serialize(listCities);
                message.Add("code", 200);
                message.Add("correct", json);
                result = Json(message, JsonRequestBehavior.AllowGet);
                return result;
            }
            else
            {
                message.Add("code", 404);
                message.Add("error", "No Document");
                Response.StatusCode = 404;
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
        }

        /// <summary>
        /// Adds the car.
        /// </summary>
        /// <returns>partial view for adding cars</returns>
        public ActionResult AddCar()
        {
            List<CarViewModel> listCarViewModel = adminService.GetCar();
            return PartialView("_AddCar", listCarViewModel);
        }
        /**
         * Returns partial view for 
         * entering inputs fields of the car
        */
        public ActionResult AddCarInput()
        {
            return PartialView("_AddCarInput");
        }

        /// <summary>
        /// Does the add car input.
        /// </summary>
        /// <param name="carViewModel">It accepts CarViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult DoAddCarInput(CarViewModel carViewModel)
        {
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            string output = CommonServices.ValidateCarViewModel(carViewModel);
            if(output.Equals("success"))
            {
                //string CurrentPath = Path.Combine(Server.MapPath("~"));
                bool flag = adminService.AddCar(carViewModel);
                if(flag)
                {
                    message.Add("code",200);
                    message.Add("success","Successfully inserted");
                    result = Json(message, JsonRequestBehavior.AllowGet);
                    return result;
                }
                message.Add("code","501");
                message.Add("error","Failed to insert into the database");
                Response.StatusCode = 501;
                Response.SuppressFormsAuthenticationRedirect = true;
                result = Json(message, JsonRequestBehavior.AllowGet);
                return result;
            }
            message.Add("code",406);
            message.Add("error", output);
            Response.StatusCode = 406;
            Response.SuppressFormsAuthenticationRedirect = true;
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Gets the car.
        /// </summary>
        /// <returns>Car Card View</returns>
        public ActionResult GetCar()
        {
            return PartialView("_GetCar");
        }

        /// <summary>
        /// Logout this instance.
        /// </summary>
        /// <returns>View of home page</returns>
        public ActionResult Logout()
        {
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            FormsAuthentication.SignOut();
            Session.Abandon();
            message.Add("code", 200);
            message.Add("success", "Logout Successfully");
            message.Add("Redirect", Url.Action("Home", "Home"));
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }
    }
}