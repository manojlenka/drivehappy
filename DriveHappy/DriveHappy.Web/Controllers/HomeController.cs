﻿using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DriveHappy.Service.Services;
using DriveHappy.Interface.IServices;
using System.Web.Script.Serialization;

namespace DriveHappy.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private IHomeServices homeService;
        private ICommonServices commonService;
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        public HomeController()
        {
            commonService = new CommonServices();
            homeService = new HomeServices();
        }

        /// <summary>
        /// Alls the car available.
        /// </summary>
        /// <returns>Partial views for all the available cars for the user</returns>
        public ActionResult AllCarAvailable()
        {
            if ((Session["CarCount"] != null) && (Session["locationName"] != null))
            {
                ViewBag.Count = Session["CarCount"].ToString();
                ViewBag.LocationName = Session["locationName"].ToString();
            }
            else
            {
                ViewBag.Count = 0;
                ViewBag.LocationName = "Here";
            }
            return PartialView("_AllCarAvailable");
        }

        /// <summary>
        /// Count the Locations.
        /// </summary>
        /// <returns>Partial view containing location Count</returns>
        public ActionResult LocationCount()
        {
            ViewBag.Count = Session["CarCount"].ToString();
            ViewBag.LocationName = Session["locationName"].ToString();
            return PartialView("_LocationCount");
        }

        /// <summary>
        /// Shows the car.
        /// </summary>
        /// <returns>Return the whole view for the user car listing</returns>
        public ActionResult ShowCar()
        {
            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");
            if (Session["locationId"] != null)
            {
                return View();
            }
            return View("Home");
        }

        /// <summary>
        /// Logins this instance.
        /// </summary>
        /// <returns>Login View</returns>
        public ActionResult Login()
        {
            return PartialView("_Login");
        }
        // GET: Home
        /// <summary>
        /// Homes this instance.
        /// </summary>
        /// <returns>Home page</returns>
        public ActionResult Home()
        {
            Response.AppendHeader("Cache-Control","no-cache, no-store, must-revalidate");
            Response.AppendHeader("Pragma","no-cache");
            Response.AppendHeader("Expires","0");
            Session["UserIdChangePassword"] = null;
            return View();
        }

        /// <summary>
        /// Signs the in.
        /// </summary>
        /// <returns>Partial view of login home page</returns>
        public PartialViewResult SignIn()
        {
            return PartialView("_SignIn");
        }

        /// <summary>
        /// Signs up.
        /// </summary>
        /// <returns>Returns the partial view for the Signup</returns>
        public PartialViewResult SignUp()
        {
            return PartialView("_SignUp");
        }

        /// <summary>
        /// Does the login.
        /// </summary>
        /// <param name="loginViewModel">The LVM.</param>
        /// <returns>Json Result</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _DoLogin(LoginViewModel loginViewModel)
        {
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if ((!string.IsNullOrEmpty(loginViewModel.Username)) || (!string.IsNullOrEmpty(loginViewModel.Password)))
            {
                string RoleWithId = homeService.GetRole(loginViewModel);
                if (RoleWithId.IndexOf(',') > 0)
                {
                    string Role = RoleWithId.Substring(0, RoleWithId.IndexOf(','));
                    int UserId;
                    bool tried = Int32.TryParse(RoleWithId.Substring(RoleWithId.IndexOf(',') + 1), out UserId);
                    if (Role.Equals("Admin"))
                    {
                        FormsAuthentication.SetAuthCookie(loginViewModel.Username, false);
                        Session["IsAdmin"] = true;
                        Session["UserId"] = UserId;
                        message.Add("success", "Admin Authentication Successful");
                        message.Add("code", 200);
                        message.Add("Redirect", Url.Action("Home", "Admin"));
                        result = Json(message, JsonRequestBehavior.AllowGet);
                        return result;
                    }
                    else if (Role.Equals("Owner"))
                    {
                        FormsAuthentication.SetAuthCookie(loginViewModel.Username, false);
                        Session["IsOwner"] = true;
                        Session["IsUser"] = true;
                        Session["UserId"] = UserId;
                        message.Add("success", "Owner Authentication Successful");
                        message.Add("code", 200);
                        if(Session["locationId"] == null)
                            message.Add("Redirect", Url.Action("Home", "Home"));
                        else
                            message.Add("Redirect", Url.Action("ShowCar", "Home"));
                        if (Session["OwnerCarBookingId"] != null)
                        {
                            int id;
                            bool tryInput = Int32.TryParse(Session["OwnerCarBookingId"].ToString(), out id);
                            return RedirectToAction("BookingDetails", "User", new { @Id = id });
                        }
                        result = Json(message, JsonRequestBehavior.AllowGet);
                        return result;
                    }
                    else if (Role.Equals("User"))
                    {
                        FormsAuthentication.SetAuthCookie(loginViewModel.Username, false);
                        Session["IsUser"] = true;
                        Session["UserId"] = UserId;
                        message.Add("success", "User Authentication Successful");
                        message.Add("code", 200);
                        if (Session["locationId"] == null)
                            message.Add("Redirect", Url.Action("Home", "Home"));
                        else
                            message.Add("Redirect", Url.Action("ShowCar", "Home"));
                        if (Session["OwnerCarBookingId"] != null)
                        {
                            int id;
                            bool tryInput = Int32.TryParse(Session["OwnerCarBookingId"].ToString(), out id);
                            return RedirectToAction("BookingDetails", "User", new { @Id = id});
                        }
                        result = Json(message, JsonRequestBehavior.AllowGet);
                        return result;
                    }
                }
            }
            message.Add("errorMismatch", "Invalid credentials");
            message.Add("code", 401);
            Response.StatusCode = 401;
            result = Json(message, JsonRequestBehavior.AllowGet);
            Response.SuppressFormsAuthenticationRedirect = true;
            return result;
        }

        /// <summary>
        /// Filters the specified filter name.
        /// </summary>
        /// <param name="filterName">Name of the filter.</param>
        /// <param name="filterCategory">The filter category.</param>
        /// <returns>Return user car model partial view</returns>
        public ActionResult Filter(string filterName, string filterCategory)
        {
            List<UserCarViewModel> filterResults = null;
            DateTime from = default(DateTime);
            DateTime to = default(DateTime);
            int locationId = default(int);
            bool flag = DateTime.TryParse(Session["from"].ToString(), out from);
            if (flag)
                flag = DateTime.TryParse(Session["to"].ToString(), out to);
            if (flag)
                flag = Int32.TryParse(Session["locationId"].ToString(), out locationId);
            if (flag)
            {
                if (filterCategory.Equals("Price"))
                {
                    filterResults = homeService.UserCar(from, to, locationId);
                    if (filterName.Equals("Low To High"))
                    {
                        filterResults = filterResults.OrderBy(m => m.CarPrice).ToList();
                    }
                    else if (filterName.Equals("High to Low"))
                    {
                        filterResults = (List<UserCarViewModel>)filterResults.OrderByDescending(m => m.CarPrice).ToList();
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    filterResults = homeService.ValidateFilter(from, to, locationId, filterName);
                }
            }
            if ((filterResults != null) && (filterResults.Count > 0))
            {
                return PartialView("_UserCarView", filterResults);
            }
            else
            {
                Dictionary<string, object> message = new Dictionary<string, object>();
                JsonResult result = new JsonResult();
                message.Add("code", 404);
                Response.StatusCode = 404;
                Response.SuppressFormsAuthenticationRedirect = true;
                message.Add("error", "content not found");
                result = Json(message, JsonRequestBehavior.AllowGet);
                return result;
            }
        }

        /// <summary>
        /// Gets the name of the car.
        /// </summary>
        /// <param name="hintCar">The hint car.</param>
        /// <returns>Car Name</returns>
        [HttpPost]
        public ActionResult GetCarName(string hintCar)
        {
            DateTime from;
            DateTime to = DateTime.Now;
            int locationId = 0;
            List<UserCarViewModel> listCarNames = null;
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            bool test = CommonServices.IsAlphaNumeric(hintCar);
            if(test)
            {
                if (Session["from"] != null)
                {
                    bool flag = DateTime.TryParse(Session["from"].ToString(), out from);
                    if (flag)
                        flag = DateTime.TryParse(Session["to"].ToString(), out to);
                    if (flag)
                        flag = Int32.TryParse(Session["locationId"].ToString(), out locationId);
                    if (flag)
                        listCarNames = homeService.GetCarNames(from, to, locationId, hintCar);
                    if ((listCarNames != null) && (listCarNames.Count > 0))
                    {
                        var jsonSerialiser = new JavaScriptSerializer();
                        string json = jsonSerialiser.Serialize(listCarNames);
                        message.Add("code", 200);
                        message.Add("result", json);
                    }
                    else
                    {
                        message.Add("code", 404);
                        message.Add("error", "No such hint found");
                        Response.StatusCode = 404;
                        Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }
                else
                {
                    message.Add("code", 404);
                    message.Add("error", "No such data exist");
                    Response.StatusCode = 404;
                    Response.SuppressFormsAuthenticationRedirect = true;
                }
            }
            else
            {
                message.Add("code", 404);
                message.Add("error", "Dangerous input found");
                Response.StatusCode = 404;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Gets the city.
        /// </summary>
        /// <param name="cityViewModel">The CVM.</param>
        /// <returns>List of cities</returns>
        [HttpPost]
        public ActionResult GetCity(CityViewModel cityViewModel)
        {
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if(cityViewModel != null)
            {
                if (cityViewModel.CityName.Length >= 2)
                {
                    List<CityViewModel> listCities = commonService.GetCity(cityViewModel);
                    if((listCities != null) && (listCities.Count > 0))
                    {
                        var jsonSerialiser = new JavaScriptSerializer();
                        string json = jsonSerialiser.Serialize(listCities);
                        message.Add("code", 200);
                        message.Add("correct", json);
                    }
                    else
                    {
                        message.Add("code", 404);
                        message.Add("error", "No Document");
                        Response.StatusCode = 404;
                        Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }
                else
                {
                    message.Add("code", 404);
                    message.Add("error", "No Document");
                    Response.StatusCode = 404;
                    Response.SuppressFormsAuthenticationRedirect = true;
                }
            }
            else
            {
                message.Add("code", 404);
                message.Add("error", "No Document");
                Response.StatusCode = 404;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Gets the location.
        /// </summary>
        /// <param name="locationHint">The location hint.</param>
        /// <returns>Return location name</returns>
        [HttpPost]
        public ActionResult GetLocation(LocationViewModel locationHint)
        {
            JsonResult result = new JsonResult();
            Dictionary<string, object> message = new Dictionary<string, object>();
            if(locationHint != null)
            {
                if (locationHint.LocationName.Length >= 1)
                {
                    List<LocationViewModel> listLocations = commonService.GetLocation(locationHint);
                    if((listLocations != null) && (listLocations.Count > 0))
                    {
                        var jsonSerialiser = new JavaScriptSerializer();
                        string json = jsonSerialiser.Serialize(listLocations);
                        message.Add("code", 200);
                        message.Add("correct", json);
                    }
                    else
                    {
                        message.Add("code", 404);
                        message.Add("error", "No Document");
                        Response.StatusCode = 404;
                        Response.SuppressFormsAuthenticationRedirect = true;
                    }
                }
                else
                {
                    message.Add("code", 404);
                    message.Add("error", "No Document");
                    Response.StatusCode = 404;
                    Response.SuppressFormsAuthenticationRedirect = true;
                }
            }
            else
            {
                message.Add("code", 404);
                message.Add("error", "No Document");
                Response.StatusCode = 404;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Does the sign up.
        /// </summary>
        /// <param name="userViewModel">The userViewModel.</param>
        /// <returns>Get sign-up content</returns>
        [HttpPost]
        public ActionResult DoSignUp(UserViewModel userViewModel)
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            string userStatus = commonService.ValidateUserRegistration(userViewModel);
            if(userStatus.Equals("success"))
            {
                homeService.RegisterUser(userViewModel);
                message.Add("code", 200);
                message.Add("success", userStatus);
            }
            else
            {
                message.Add("code", 406);
                message.Add("error", userStatus);
                Response.StatusCode = 406;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Activates the account.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Activate account after clicking the activation link</returns>
        public ActionResult ActivateAccount(string id)
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            if(!string.IsNullOrEmpty(id))
            {
                bool statusMessage = homeService.ConfirmAccountActivation(id);
                if(statusMessage)
                {
                    return View("Home");
                }
                else
                {
                    return View("Home");
                }
            }
            else
            {
                return View("Home");
            }
        }

        /// <summary>
        /// Forgets the password.
        /// </summary>
        /// <returns>Return partial view for the forget password</returns>
        public ActionResult ForgetPassword()
        {
            return PartialView("_ForgetPassword");
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Returns home after setting session id</returns>
        public ActionResult ResetPassword(string id)
        {
            int? userId = commonService.ValidateUniqueId(id);
            if (userId.HasValue)
            {
                Session["UserIdChangePassword"] = (userId ?? 0).ToString();
                return View("Home");
            }
            return View("Home");
        }

        /// <summary>
        /// Does the reset password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns>Return json after reseting password</returns>
        [HttpPost]
        public ActionResult DoResetPassword(string password)
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            bool strongPassword = commonService.IsStrongPassword(password);
            if(strongPassword)
            {
                string userIdChangePassword = Session["UserIdChangePassword"].ToString();
                if (!string.IsNullOrEmpty(userIdChangePassword))
                {
                    int userId;
                    bool tryFlag = Int32.TryParse(userIdChangePassword, out userId);
                    if (tryFlag)
                    {
                        bool flag = homeService.ResetPassword(password, userId);
                        if(flag)
                        {
                            message.Add("code", 200);
                            message.Add("success", "Password changed successfully");
                        }
                        else
                        { 
                            message.Add("code", 406);
                            Response.StatusCode = 406;
                            Response.SuppressFormsAuthenticationRedirect = true;
                            message.Add("error", "Link has expired");
                        }
                    }
                    result = Json(message, JsonRequestBehavior.AllowGet);
                    return result;
                }
                return View("Home");
            }
            message.Add("code", 406);
            message.Add("error", "Password is not strong");
            Response.StatusCode = 406;
            Response.SuppressFormsAuthenticationRedirect = true;
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Does the forget password.
        /// </summary>
        /// <param name="userViewModel">The userViewModel.</param>
        /// <returns>Json result</returns>
        [HttpPost]
        public ActionResult DoForgetPassword(UserViewModel userViewModel)
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            string validationStatus = commonService.ValidateForgetPasswordData(userViewModel);
            if(validationStatus.Equals("success"))
            {
                bool flag = homeService.DoForgetPassword(userViewModel);
                if(flag)
                {
                    message.Add("code", 200);
                    message.Add("success","success");
                }
                else
                {
                    message.Add("code", 400);
                    message.Add("error", "Bad Request");
                    Response.StatusCode = 400;
                    Response.SuppressFormsAuthenticationRedirect = true;
                }
                result = Json(message, JsonRequestBehavior.AllowGet);
                return result;
            }
            message.Add("code", 400);
            message.Add("error", "Bad Request");
            Response.StatusCode = 400;
            Response.SuppressFormsAuthenticationRedirect = true;
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Cars the listing.
        /// </summary>
        /// <returns>Partial view of car</returns>
        public ActionResult CarListing()
        {
            List<UserCarViewModel> userCarList = null;
            DateTime from;
            DateTime to = DateTime.Now;
            int locationId = 0;
            if(Session["from"] != null)
            {
                bool flag = DateTime.TryParse(Session["from"].ToString(), out from);
                if (flag)
                    flag = DateTime.TryParse(Session["to"].ToString(), out to);
                if (flag)
                    flag = Int32.TryParse(Session["locationId"].ToString(), out locationId);
                if (flag)
                    userCarList = homeService.UserCar(from, to, locationId);
                Session["CarCount"] = userCarList.Count.ToString();
            }
            return PartialView("_UserCarView", userCarList);
        }

        /// <summary>
        /// Bookings the details.
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>Booking details page if login else login page</returns>
        public ActionResult BookingDetails(int? Id)
        {
            if(Id.HasValue)
            {
                if ((Session["IsUser"]  != null) && (Convert.ToBoolean(Session["IsUser"].ToString())))
                {
                    return RedirectToAction("BookingDetails", "User", new { @Id= Id});
                }
                else
                {
                    Dictionary<string, object> message = new Dictionary<string, object>();
                    JsonResult result = new JsonResult();
                    message.Add("code", 302);
                    message.Add("result", "login");
                    Session["OwnerCarBookingId"] = Id ?? 0;
                    result = Json(message, JsonRequestBehavior.AllowGet);
                    return result;
                }
            }
            return RedirectToAction("CarListing", "Home");
        }

        /// <summary>
        /// Logout this instance.
        /// </summary>
        /// <returns>home page after logout</returns>
        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            //HttpCookie AuthCookie = new HttpCookie(".ASPXAUTH");
            //AuthCookie.Value = "";
            Response.AddHeader("Set-Cookie", ".ASPXAUTH=''");
            return RedirectToAction("Home", "Home");
        }

        /// <summary>
        /// Schedulers the data.
        /// </summary>
        /// <param name="scheduler">The scheduler.</param>
        /// <returns>Partial view for scheduler</returns>
        public ActionResult SchedulerData(SchedulerViewModel scheduler)
        {
            string schedulerStatus = commonService.ValidateScheduler(scheduler);
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            if(schedulerStatus.Contains("success"))
            {
                int locationId;
                bool flag = Int32.TryParse(schedulerStatus.Substring(7), out locationId);
                if(flag)
                {
                    DateTime from = scheduler.FromDate.Date + scheduler.FromTime.TimeOfDay;
                    DateTime to = scheduler.ToDate.Date + scheduler.ToTime.TimeOfDay;
                    Session["to"] = to;
                    Session["from"] = from;
                    Session["locationId"] = locationId;
                    Session["locationName"] = scheduler.LocationName;
                    message.Add("code", 200);
                    message.Add("correct", "/Home/CarListing");
                }
                else
                {
                    message.Add("code", 400);
                    message.Add("error", "Invalid Data");
                    Response.StatusCode = 400;
                    Response.SuppressFormsAuthenticationRedirect = true;
                }
            }
            else
            {
                message.Add("code", 400);
                message.Add("error", schedulerStatus);
                Response.StatusCode = 400;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

    }
}