﻿using DriveHappy.Data.ViewModel;
using DriveHappy.Service.Services;
using DriveHappy.Web.CustomAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DriveHappy.Interface.IServices;
using System.Web.Security;
using System.Web.Script.Serialization;

namespace DriveHappy.Web.Controllers
{
    [Authorize]
    [OwnerAuthorizeFilter]
    public class OwnerController : Controller
    {
        private IOwnerServices OwnerService;
        private ICommonServices CommonService;
        /// <summary>
        /// Initializes a new instance of the <see cref="OwnerController"/> class.
        /// </summary>
        public OwnerController()
        {
            OwnerService = new OwnerServices();
            CommonService = new CommonServices();
        }

        /// <summary>
        /// Mies the cars.
        /// </summary>
        /// <returns>partial view for the Owner Car listing</returns>
        public ActionResult MyCars()
        {
            List<CarViewModel> listCarViewModel = new List<CarViewModel>();
            listCarViewModel = OwnerService.GetCar((int)Session["UserId"]);
            return PartialView("_MyCars", listCarViewModel);
        }

        /// <summary>
        /// Noes the car added.
        /// </summary>
        /// <returns>Partial view for the Owner when no car are added</returns>
        public ActionResult NoCarAdded()
        {
            return PartialView("_NoCarAdded");
        }

        /// <summary>
        /// Cars the data.
        /// </summary>
        /// <returns>partial view for the Car Details entering option</returns>
        public ActionResult CarData()
        {
            return PartialView("_CarData");
        }

        /// <summary>
        /// Validates the date.
        /// </summary>
        /// <param name="ownerCarViewModel">The ownerCarViewModel.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ValidateDate(OwnerCarViewModel ownerCarViewModel)
        {
            string dateStatus = CommonService.ValidateDate(ownerCarViewModel.FromDate, ownerCarViewModel.ToDate);
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            if(dateStatus.Equals("success"))
            {
                message.Add("code", 200);
                message.Add("success", dateStatus);
            }
            else
            {
                message.Add("code", 406);
                message.Add("error", dateStatus);
                Response.StatusCode = 406;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Validates the owner car data.
        /// </summary>
        /// <param name="ownerCarViewModel">The ownerCarViewModel.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ValidateOwnerCarData(OwnerCarViewModel ownerCarViewModel)
        {
            string carStatus = CommonService.ValidateCarDetails(ownerCarViewModel);
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            if (carStatus.Equals("success"))
            {
                message.Add("code", 200);
                message.Add("success", carStatus);
            }
            else
            {
                message.Add("code", 406);
                message.Add("error", carStatus);
                Response.StatusCode = 406;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Validates the location.
        /// </summary>
        /// <param name="ownerCarViewModel">The ownerCarViewModel.</param>
        /// <returns>json result if location is valid of not</returns>
        [HttpPost]
        public ActionResult ValidateLocation(OwnerCarViewModel ownerCarViewModel)
        {
            string locationStatus = CommonService.ValidateLocation(ownerCarViewModel);
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            if (locationStatus.Equals("success"))
            {
                message.Add("code", 200);
                message.Add("success", locationStatus);
            }
            else
            {
                message.Add("code", 406);
                message.Add("error", locationStatus);
                Response.StatusCode = 406;
                Response.SuppressFormsAuthenticationRedirect = true;
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Owners the car add.
        /// </summary>
        /// <param name="ownerCarViewModel">The owner car view model.</param>
        /// <returns>json result</returns>
        public ActionResult OwnerCarAdd(OwnerCarViewModel ownerCarViewModel)
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            string validateMessage = CommonService.ValidateOwnerCarData(ownerCarViewModel);
            if(validateMessage.Equals("success"))
            {
                ownerCarViewModel.UserId = (int)Session["UserId"];
                string statusMessage = OwnerService.AddOwnerCar(ownerCarViewModel);
                if(statusMessage.Equals("success"))
                {
                    message.Add("code","200");
                    message.Add("success","success");
                    result = Json(message, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    message.Add("code","406");
                    Response.StatusCode = 406;
                    message.Add("error", statusMessage);
                    result = Json(message, JsonRequestBehavior.AllowGet);
                    Response.SuppressFormsAuthenticationRedirect = true;
                }
                return result;
            }
            else
            {
                message.Add("code","406");
                Response.StatusCode = 406;
                message.Add("error", validateMessage);
                result = Json(message, JsonRequestBehavior.AllowGet);
                Response.SuppressFormsAuthenticationRedirect = true;
                return result;
            }
        }

        /// <summary>
        /// Gets the car brand.
        /// </summary>
        /// <param name="brandHint">The brand hint.</param>
        /// <returns>Json result contain list of brand name</returns>
        public ActionResult GetCarBrand(string BrandHint)
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            List<string> brandNames = null;
            if(CommonServices.IsAlphaNumeric(BrandHint))
            {
                if (BrandHint.Length > 1)
                {
                    brandNames = OwnerService.GetBrandNameList(BrandHint);
                    if((brandNames != null) && (brandNames.Count > 0))
                    {
                        message.Add("code", 200);
                        message.Add("correct", brandNames);
                    }
                    else
                    {
                        Response.StatusCode = 404;
                        Response.SuppressFormsAuthenticationRedirect = true;
                        message.Add("code", 404);
                        message.Add("error", "Page not found");
                    }
                }
                else
                {
                    Response.StatusCode = 404;
                    Response.SuppressFormsAuthenticationRedirect = true;
                    message.Add("code", 404);
                    message.Add("error", "Page not found");
                }
            }
            else
            {
                Response.StatusCode = 404;
                Response.SuppressFormsAuthenticationRedirect = true;
                message.Add("code", 400);
                message.Add("error", "Dangerous Request");
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /// <summary>
        /// Gets the car model.
        /// </summary>
        /// <param name="ModelHint">The model hint.</param>
        /// <param name="BrandName">Name of the brand.</param>
        /// <returns>List of Model name having hint as Model hint of the specified Brand Name</returns>
        public ActionResult GetCarModel(string ModelHint, string BrandName)
        {
            Dictionary<string, object> message = new Dictionary<string, object>();
            JsonResult result = new JsonResult();
            List<string> modelNames = null;
            if ((CommonServices.IsAlphaNumeric(BrandName)) && (CommonServices.IsAlphaNumeric(ModelHint)))
            {
                if (ModelHint.Length > 0)
                {
                    modelNames = OwnerService.GetModelNameList(ModelHint, BrandName);
                    if ((modelNames != null) && (modelNames.Count > 0))
                    {
                        message.Add("code", 200);
                        message.Add("correct", modelNames);
                    }
                    else
                    {
                        Response.StatusCode = 404;
                        Response.SuppressFormsAuthenticationRedirect = true;
                        message.Add("code", 404);
                        message.Add("error", "Page not found");
                    }
                }
                else
                {
                    Response.StatusCode = 404;
                    Response.SuppressFormsAuthenticationRedirect = true;
                    message.Add("code", 404);
                    message.Add("error", "Page not found");
                }
            }
            else
            {
                Response.StatusCode = 404;
                Response.SuppressFormsAuthenticationRedirect = true;
                message.Add("code", 400);
                message.Add("error", "Dangerous Request");
            }
            result = Json(message, JsonRequestBehavior.AllowGet);
            return result;
        }

        /**
         * Return Home page
         * Clear authentication cookie
         * And session is abandon
        **/
        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            //HttpCookie AuthCookie = new HttpCookie(".ASPXAUTH");
            //AuthCookie.Value = "";
            Response.AddHeader("Set-Cookie", ".ASPXAUTH=''");
            return RedirectToAction("Home","Home");
        }
    }
}