﻿using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IRepository
{
    public interface IUserRepository
    {
        IDbConnection GetConnection { get; }
        CarBookingViewModel GetBookingData(int OwnerCarId, int LocationId);
        int GetOwnerCarId(int OwnerCarId);
    }
}
