﻿using DriveHappy.Data.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IRepository
{
    public interface IOwnerRepository
    {
        IDbConnection GetConnection { get; }
        List<Car> GetOwnerCar(int OwnerUserId);
        int? GetCityId(City cityModel);
        int? GetLocationId(Location location);
        List<string> GetBrandNameList(string BrandHint);
        List<string> GetModelNameList(string ModelHint, string BrandName);
    }
}
