﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IRepository
{
    /**
     * IRepository interface
     * Containing abstract method for CRUD operations
     * Contain generic methods
    */
    public interface IRepository
    {
        /**
         * Returns connection string
        */
        IDbConnection GetConnection { get; }
        /**
         * Method for getting list of record
         * from the database
         * 
         * Accepts one argument of type object.
         * And return the List of specified type.
        */ 

        List<T> GetListData<T>(Object o, string StoredProcedureName);
        List<T> GetListData<T>(string StoredProcedure);

        /**
         * Return Id
        */
        int? GetId(Object o, string StoredProcedureName);
        T GetSingleData<T>(Object o, string ProcedureName);
        /**
         * void AddRecord(Object o);
        */
        //bool AddManyRecord(Object[] o);

        bool AddSingleRecord(Object o, string StoredProcedureName);
        bool UpdateRecord(Object o, string StoredProcedureName);

        T ExecuteQuery<T>(string query);
        List<T> ExecuteManyQuery<T>(string query);
    }
}