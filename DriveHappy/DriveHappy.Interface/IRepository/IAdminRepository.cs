﻿using DriveHappy.Data.Models;
using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IRepository
{
    public interface IAdminRepository
    {
        IDbConnection GetConnection { get; }
        bool AddRole(Role role);
        int? GetId(LookUp lookUp);
        bool AddLookUpValue(LookUp lookUp);
        int? GetValueId(Value val);
        bool AddCategory(Value val);
        List<Value> GetValue(Value val);
        int? GetCityId(City cityModel);
        bool AddCity(City cityModel);
        bool AddLocation(Location locationModel);
        List<City> GetCity(City cityModel);
        int? CountOwnerCar();
        List<Car> GetCar();
        List<CarVerificationViewModel> GetCarVerificationDetail(int OwnerCarId);
        bool VerifyCar(int OwnerCarId);
        bool RejectCar(int OwnerCarId);
    }
}
