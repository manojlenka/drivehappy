﻿using DriveHappy.Data.Models;
using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DriveHappy.Interface.IRepository
{
    public interface IHomeRepository
    {
        IDbConnection GetConnection { get; }
        string GetSalt(string Username);
        int? GetUserId(string username, string password);
        bool RegisterUser(UserViewModel user, string Username, string Salt, string UniqueId);
        bool UpdateUniqueKey(string UniqueId, string Email);
        int VerifyPlateNo(string PlateNo);
        int VerifyEmail(string Email);
        int? GetCityId(City cityModel);
        int? GetLocationId(Location location);
        int? GetUserChangePassword(string Username, string Email);
        int? GetUserIdByUniqueId(string UniqueId);
        List<City> GetListCity(City city);
    }
}