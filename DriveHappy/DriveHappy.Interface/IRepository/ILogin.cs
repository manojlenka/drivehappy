﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IRepository
{
    /**
     * IRepository interface
     * Containing abstract method for CRUD operations
     * Contain generic methods
    */
    public interface ILogin
    {
        /**
         * Method for getting list of record
         * from the database
         * 
         * Accepts one argument of type object.
         * And return the List of specified type.
        */ 
        List<T> GetDetails<T>(Object o);
    }
}