﻿using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IServices
{
    public interface ICommonServices
    {
        /**
         * Functions for server side data validation
         * if the data is correct then returns true
         * else false.
        */ 
        bool IsPhoneNumber(string number);
        bool IsEmail(string Email);
        bool IsBirthDate(DateTime Date);
        //bool IsName(string Name);
        bool IsStrongPassword(string Password);

        List<CityViewModel> GetCity(CityViewModel cvm);
        List<LocationViewModel> GetLocation(LocationViewModel locationHint);

        //string GetAppSettings();
        string ValidateOwnerCarData(OwnerCarViewModel ocvm);
        string ValidateDate(DateTime from, DateTime to);
        string ValidateLocation(OwnerCarViewModel ocvm);
        string ValidateCarDetails(OwnerCarViewModel ocvm);
        string ValidateUserRegistration(UserViewModel uvm);
        string ValidateForgetPasswordData(UserViewModel uvm);
        string ValidateScheduler(SchedulerViewModel scheduler);

        int? ValidateUniqueId(string id);
    }
}
