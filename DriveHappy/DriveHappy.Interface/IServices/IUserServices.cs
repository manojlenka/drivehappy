﻿using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IServices
{
    public interface IUserServices
    {
        CarBookingViewModel GetCarBookingDetails(int ownerCarId, int locationId, DateTime from, DateTime to);
    }
}
