﻿using DriveHappy.Data.Models;
using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IServices
{
    public interface IHomeServices
    {
        /**
         * Getting roles for the Login
         * Return role
        */ 
        string GetRole(LoginViewModel lvm);
        /**
         * Registering new user
         * returns name of the user
        */ 
        string RegisterUser(UserViewModel uvm);

        bool ConfirmAccountActivation(string Id);

        bool DoForgetPassword(UserViewModel uvm);

        bool ResetPassword(string password, int userId);

        List<UserCarViewModel> UserCar(DateTime from, DateTime to, int locationId);

        List<UserCarViewModel> GetCarNames(DateTime from, DateTime to, int locationId, string hintCar);

        List<UserCarViewModel> ValidateFilter(DateTime from, DateTime to, int locationId, string filterName);
    }
}
