﻿using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IServices
{
    public interface IAdminServices
    {
        /**
         * Methods for adding respective data
        */ 
        bool AddRole(RoleViewModel[] rvm);
        bool AddCategory(ValueViewModel[] vvm);        
        bool AddFuel(ValueViewModel[] vvm);
        bool AddSeater(ValueViewModel[] vvm);
        bool AddTransmission(ValueViewModel[] vvm);
        bool AddLocation(LocationViewModel[] lvm);
        bool AddCar(CarViewModel cvm);
        bool VerifyCar(int id);
        bool RejectCar(int id);

        ///**
        // * Methods for getting respective data
        //*/ 
        List<ValueViewModel> GetValue(LookUpViewModel luvm);
        List<CarViewModel> GetCar();
        List<CityViewModel> GetCity(CityViewModel cvm);
        List<AdminCarDataViewModel> GetAdminCarData(int pageNo);
        List<AdminOwnerDataViewModel> GetAdminOwnerData();

        CarVerificationViewModel GetCarVerificationViewData(int ownerCarId);

        int AdminCarMaxPages();
    }
}
