﻿using DriveHappy.Data.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveHappy.Interface.IServices
{
    /// <summary>
    /// Contains IOwnerServices interface method
    /// </summary>
    public interface IOwnerServices
    {
        /// <summary>
        /// Adds the owner car.
        /// </summary>
        /// <param name="ocvm">The ocvm.</param>
        /// <returns></returns>
        string AddOwnerCar(OwnerCarViewModel ocvm);
        /// <summary>
        /// Gets the car.
        /// </summary>
        /// <param name="UserId">The user identifier.</param>
        /// <returns></returns>
        List<CarViewModel> GetCar(int UserId);

        List<string> GetBrandNameList(string BrandHint);

        List<string> GetModelNameList(string ModelHint, string BrandName);
    }
}
