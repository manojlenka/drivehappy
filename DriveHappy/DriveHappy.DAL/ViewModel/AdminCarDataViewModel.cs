﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class AdminCarDataViewModel
    {
        public int OwnerCarId { get; set; }
        public string CarName { get; set; }
        public string Username { get; set; }
        public string CityName { get; set; }
        public DateTime AddedOn { get; set; }
        public byte Verified { get; set; }
        public int nowPage { get; set; }
        public int maxPage { get; set; }
    }
}