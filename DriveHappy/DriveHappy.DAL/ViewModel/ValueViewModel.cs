﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class ValueViewModel
    {
        public int ValueId { get; set; }
        public string ValueName { get; set; }
    }
}