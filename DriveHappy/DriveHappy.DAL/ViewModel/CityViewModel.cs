﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class CityViewModel
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }
}