﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class LocationViewModel
    {
        public int LocationId { get; set; }
        public string CityName { get; set; }
        public string LocationName { get; set; }
    }
}