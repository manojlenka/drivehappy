﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class CarVerificationViewModel
    {
        public int OwnerCarId { get; set; }
        public string Username { get; set; }
        public string CarPath { get; set; }
        public string BrandName { get; set; }
        public string CarName { get; set; }
        public DateTime AddedOn { get; set; }
        public string Email { get; set; }
        public DateTime AvailableFrom { get; set; }
        public DateTime AvailableTo { get; set; }
        public decimal HourlyPrice { get; set; }
        public decimal DailyPrice { get; set; }
        public decimal SplDayPrice { get; set; }
        public string PlateNo { get; set; }
    }
}