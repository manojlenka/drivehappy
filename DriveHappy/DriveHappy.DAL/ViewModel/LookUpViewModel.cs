﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class LookUpViewModel
    {
        public int LookUpId { get; set; }
        public string LookUpValue { get; set; }
    }
}