﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class OwnerCarViewModel
    {
        public int UserId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public Decimal HourlyPrice { get; set; }
        public Decimal DailyPrice { get; set; }
        public Decimal SplDayPrice { get; set; }
        public string PlateNo { get; set; }
        public string CarBrand { get; set; }
        public string CarModel { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
    }
}