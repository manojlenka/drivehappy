﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class CarViewModel
    {
        public int CarId { get; set; }
        public string CarName { get; set; }
        public string CarCategory { get; set; }
        public string FuelType { get; set; }
        public string Seater { get; set; }
        public string BrandName { get; set; }
        public string Transmission { get; set; }
        public HttpPostedFileBase CarImage { get; set; }
        public string CarPath { get; set; }
    }
}