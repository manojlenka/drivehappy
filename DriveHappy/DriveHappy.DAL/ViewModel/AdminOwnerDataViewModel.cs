﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class AdminOwnerDataViewModel
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string CityName { get; set; }
        public DateTime CreatedOn { get; set; }
        public byte Verification { get; set; }
    }
}