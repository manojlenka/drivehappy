﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class SchedulerViewModel
    {
        public string CityName { get; set; }
        public string LocationName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
    }
}