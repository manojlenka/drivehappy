﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    /**
     * View Model binds the login of the user
    */
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Invalid Credential")]
        [Display(Name = "username")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Invalid Credential")]
        [Display(Name = "password")]
        public string Password { get; set; }
    }
}