﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class UserCarViewModel
    {
        public int OwnerCarId { get; set; }
        public decimal CarPrice { get; set; }
        public string CarName { get; set; }
        public string CarCategory { get; set; }
        public string FuelType { get; set; }
        public int Seater { get; set; }
        public string BrandName { get; set; }
        public string Transmission { get; set; }
        public string CarPath { get; set; }
        public Decimal HourlyPrice { get; set; }
        public Decimal DailyPrice { get; set; }
    }
}