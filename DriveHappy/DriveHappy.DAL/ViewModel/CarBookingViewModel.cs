﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.ViewModel
{
    public class CarBookingViewModel
    {
        public int SitePickUpId { get; set; }
        public string CarPath { get; set; }
        public string CarName { get; set; }
        public string BrandName { get; set; }
        public string LocationName { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public decimal CarPrice { get; set; }
        public decimal DailyPrice { get; set; }
        public decimal HourlyPrice { get; set; }
        public decimal Tax { get; set; }
        public decimal NetAmount { get; set; }
        public int Day { get; set; }
        public int Hour { get; set; }
    }
}