﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep Registered User details
    */
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Pin { get; set; }
        public DateTime DOB { get; set; }
        public Boolean Verification { get; set; }
        public string UserImage { get; set; }
        public string UniqueId { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}