﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep Transaction details
    */
    public class Transaction
    {
        public int TransactionId { get; set; }
        public string PaymentType { get; set; }
        public string Gateway { get; set; }

        public int BookingId { get; set; }
    }
}