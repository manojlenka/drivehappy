﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep lookup values
    */
    public class LookUp
    {
        public int LookUpId { get; set; }
        public string LookUpValue { get; set; }
    }
}