﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep details about the Owner Car
    */
    public class OwnerCar
    {
        public int UserId { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Decimal HourlyPrice { get; set; }
        public Decimal DailyPrice { get; set; }
        public Decimal SplDayPrice { get; set; }
        public string PlateNo { get; set; }
        public string CarBrand { get; set; }
        public string CarModel { get; set; }
        public int CityId { get; set; }
        public int LocationId { get; set; }
    }
}