﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep User Roles 
    */
    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}