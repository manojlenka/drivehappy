﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep Car details
    */
    public class Car
    {
        public int CarId { get; set; }
        public string CarName { get; set; }
        public string CarCategory { get; set; }
        public string FuelType { get; set; }
        public int Seater { get; set; }
        public string BrandName { get; set; }
        public string Transmission { get; set; }
        public string CarImage { get; set; }
    }
}