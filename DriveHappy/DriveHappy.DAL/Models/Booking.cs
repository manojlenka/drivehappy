﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep Booking data details
    */
    public class Booking
    {
        public int BookingId { get; set; }
        public int UserId { get; set; }
        public DateTime BookingFrom { get; set; }
        public DateTime BookingTo { get; set; }
        public Decimal SecurityDeposit { get; set; }
        public int StatusId { get; set; }
        public Decimal Amount { get; set; }
    }
}