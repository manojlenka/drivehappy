﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    public class UserCar
    {
        public int OwnerCarId { get; set; }
        public decimal HourlyPrice { get; set; }
        public decimal DailyPrice { get; set; }
        public decimal SplDayPrice { get; set; }
        public string CarName { get; set; }
        public string CarCategory { get; set; }
        public string FuelType { get; set; }
        public string Seater { get; set; }
        public string BrandName { get; set; }
        public string Transmission { get; set; }
        public string CarPath { get; set; }
    }
}