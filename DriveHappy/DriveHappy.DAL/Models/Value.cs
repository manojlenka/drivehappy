﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep LookUp table values
    */
    public class Value
    {
        public int ValueId { get; set; }
        public int LookUpId { get; set; }
        public string ValueName { get; set; }
    }
}