﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Intermediate class for mapping many to many
     * relationship between Car and Location
    */
    public class SitePickup
    {
        public int SitePickupId { get; set; }
        public int CarId { get; set; }
        public int LocationId { get; set; }
    }
}