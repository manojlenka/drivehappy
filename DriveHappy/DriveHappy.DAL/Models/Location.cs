﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * Keep Location details
    */
    public class Location
    {
        public int LocationId { get; set; }
        public int CityId { get; set; }
        public string LocationName { get; set; }
    }
}