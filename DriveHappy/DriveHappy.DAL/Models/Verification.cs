﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriveHappy.Data.Models
{
    /**
     * keep details of owner verification list
    */
    public class Verification
    {
        public int VerificationId { get; set; }
        public int VerificationItem { get; set; }
        public string DrivingLicense { get; set; }
        public string Item { get; set; }
    }
}